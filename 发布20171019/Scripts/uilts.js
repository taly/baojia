﻿function PostApi(url, data, success, error, contenttype) {
    contenttype = contenttype || "application/x-www-form-urlencoded";
    $.ajax({
        url: url,
        type: "post",
        data: data,
        dataType: "json",
        contentType:contenttype,
        success: function (result) {
            success(result);
        },
        error: function () {
            error();
        }
    });
}

function PostFile(url, data, success, error) {
    $.ajax({
        url: url,
        type: "post",
        data: data,
        dataType: "json",
        contentType: false,
        processData:false,
        success: function (result) {
            success(result);
        },
        error: function () {
            error();
        }
    });
}

function ValidateModel(selector, failed) {
    selector = selector || "";
    var isvalid = true;
    var allform = $(selector + " input:not(input[type='button'],input[type='submit'],input[type='hidden'],input[type='radio'])");
    if (allform != undefined && allform.length > 0) {
        $.each(allform, function (index, value) {
            var obj = $(value);
            var value = obj.val().trim();
            var type = obj.attr("type");
            var error = obj.attr("data-error").trim() || "格式错误!";
            var datatype = obj.attr("data-type");
            if (type == "text" || type == "password") {
                if (value == "" && obj.attr("required") == "required") {
                    failed(error);
                    isvalid = false;
                    return false;
                }
                if (datatype == "text") {
                    var minlength = obj.attr("minlength");
                    var maxlength = obj.attr("maxlength");
                    if (minlength != undefined && minlength > value.length) {
                        failed(error);
                        isvalid = false;
                        return false;
                    }
                    if (maxlength != undefined && maxlength < value.length) {
                        failed(error);
                        isvalid = false;
                        return false;
                    }
                } else if (datatype == "number") {
                    if (!/(^[1-9]([0-9]+)?(\.[0-9]{1,3})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/.test(value)) {
                        failed(error);
                        isvalid = false;
                        return false;
                    }
                    var min = obj.attr("min");
                    var max = obj.attr("max");
                    if (min != undefined && parseFloat(min) > parseFloat(value)) {
                        failed(error);
                        isvalid = false;
                        return false;
                    }
                    if (max != undefined && parseFloat(max) < parseFloat(value)) {
                        failed(error);
                        isvalid = false;
                        return false;
                    }
                } else if (datatype == "datetime") {
                   
                }

            }
        })
    }
    return isvalid;
}

function GetFormData(selector) {
    var data = {};
    selector = selector || "";
    var alltextform = $(selector + " input:not(input[type='button'],input[type='submit'])");
    if (alltextform.length > 0) {
        $.each(alltextform, function (index, value) {
            var name = $(value).attr("name");
            if (name != undefined && name != "") {
                data[name] = ($(value).val().trim() || "");
            }
        });
    }
    var allselectform = $(selector + " select");
    if (allselectform.length > 0) {
        $.each(allselectform, function (index, value) {
            var name = $(value).attr("name");
            if (name != undefined && name != "") {
                data[name] = ($(value).val().trim() || "");
            }
        });
    }
    return data;
}

function EmptyForm(selector) {
    selector = selector || "";
    var allform = $(selector + " input:not(input[type='button'],input[type='submit'])");
    if (allform.length > 0) {
        $.each(allform, function (index, value) {
            $(value).val("");
        });
    }
}

$(function () {
    $("input[type='text']").keydown(function (e) {
        var txt = $(e.target).val();
        switch (e.key) {
            case "F2":
                $(e.target).val(txt + "//");
                break;
            case "F3":
                $(e.target).val(txt + "⊥");
                break;
            case "F4":
                $(e.target).val(txt + "▽");
                break;
            case "F5":
                $(e.target).val(txt + "±");
                break;
            case "F6":
                $(e.target).val(txt + "°");
                break;
            case "F7":
                $(e.target).val(txt + "↗");
                break;
            case "F8":
                $(e.target).val(txt + "Φ");
                break;
            case "F9":
                $(e.target).val(txt + "δ");
                break;
            case "F10":
                $(e.target).val(txt + "∠");
                break;
            case "F11":
                $(e.target).val(txt + "匚");
                break;
            default:
                return true;
        }
        return false;
    });
})

function uncheckAll($table) {
    var select_tr = $("table tr[data-index].act");
    $.each(select_tr, function (index, value) {
        $table.bootstrapTable("uncheck", $(value).attr("data-index"));
    });
}

function convertTime(ms_date) {
    if (ms_date == null || ms_date == "/Date(-62135596800000)/")
        return null
    var date = new Date(parseInt(ms_date.substr(6)));
    return date.getFullYear() + '-' + (date.getMonth() + 1).toString().padStart(2, '0') + '-' + date.getDate().toString().padStart(2, '0');
}
function GetQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}