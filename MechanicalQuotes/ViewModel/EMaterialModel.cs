﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.ViewModel
{
    /// <summary>
    /// 材料型号
    /// </summary>
    public enum EMaterialModel
    {
        /// <summary>
        /// 板料
        /// </summary>
        Panel = 1,
        /// <summary>
        /// 棒料
        /// </summary>
        Bar = 2,
        /// <summary>
        /// 圆管料
        /// </summary>
        Tube = 3,
        /// <summary>
        /// 方钢管
        /// </summary>
        SquareSteelTube = 4,
        /// <summary>
        /// 特殊钢
        /// </summary>
        SpecialSteel = 5,
        /// <summary>
        /// 毛胚或者其他不规则材料
        /// </summary>
        Other = 6
    }
}
