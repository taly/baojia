﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.ViewModel
{
    public enum ResultCode
    {
        Failed = -1,
        Success = 0,
        UnAuthorized = 401,
        ServerException = 500,
        NoPermission = 400
    }
}
