﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.ViewModel
{
    public class V_ProductTree_Part
    {
        public V_ProductTree_Part()
        {
            //childs = new List<V_ProductTree_Part>();
        }

        /// <summary>
        /// 产品零件编号
        /// </summary>
        public int pp_id { get; set; }

        /// <summary>
        /// 零件序号
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 零件编号
        /// </summary>
        public string sno { get; set; }
        /// <summary>
        /// 零件名
        /// </summary>
        public string name { get; set; }

        public IList<V_ProductTree_Part> childs;
    }
}
