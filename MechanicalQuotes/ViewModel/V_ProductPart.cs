﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.ViewModel
{
    public class V_ProductPart
    {
        public int part_id { get; set; }

        public int product_id { get; set; }

        public int pp_id { get; set; }

        public string sno { get; set; }

        public string name { get; set; }

        public int qty { get; set; }

        public double weight { get; set; }
    }
}
