﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.ViewModel
{
    public class OperateResult
    {
        public ResultCode code { get; set; }

        public string message { get; set; }
    }

    public class OperateResult<T>
    {
        public ResultCode code { get; set; }

        public string message { get; set; }

        public T data { get; set; }
    }
}
