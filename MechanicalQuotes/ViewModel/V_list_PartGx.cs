﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.ViewModel
{
    public class V_list_PartGx
    {
        /// <summary>
        /// 零件工序编号
        /// </summary>
        public int pg_id { get; set; }

        /// <summary>
        /// 工序名称
        /// </summary>
        public string gx_name { get; set; }

        /// <summary>
        /// 工艺术语
        /// </summary>
        public string gy_shuyu { get; set; }

        public int product_id { get; set; }

        public int part_id { get; set; }

        public int pp_id { get; set; }

        /// <summary>
        /// 准备工时
        /// </summary>
        public double prepare_time { get; set; }
        /// <summary>
        /// 单件工时
        /// </summary>
        public double single_time { get; set; }
        /// <summary>
        /// 热重比
        /// </summary>
        public double hot_percent { get; set; }
        /// <summary>
        /// 操作员
        /// </summary>
        public string employee { get; set; }
    }
}
