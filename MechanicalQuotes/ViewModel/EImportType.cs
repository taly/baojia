﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.ViewModel
{
    /// <summary>
    /// 导入的文件
    /// </summary>
    public enum EImportType
    {
        /// <summary>
        /// 原材料
        /// </summary>
        Raw = 1,
        /// <summary>
        /// 工序
        /// </summary>
        GongXu = 2,
        /// <summary>
        /// 其他材料
        /// </summary>
        OtherRm = 3,
        /// <summary>
        /// 工艺
        /// </summary>
        GongYi = 4,
        /// <summary>
        /// 产品
        /// </summary>
        Product = 5
    }
}
