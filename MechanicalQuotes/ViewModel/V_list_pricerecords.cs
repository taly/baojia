﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.ViewModel
{
    public class V_list_pricerecords
    {
        public int pr_id { get; set; }

        public string name { get; set; }

        public decimal price { get; set; }


        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime update_time { get; set; }

        /// <summary>
        /// 修改人
        /// </summary>
        public string update_user { get; set; }
    }
}
