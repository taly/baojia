﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.ViewModel
{
    public class V_ProductTree
    {

        public V_ProductTree()
        {
            Parts = new List<V_ProductTree_Part>();
        }
        /// <summary>
        /// 产品id
        /// </summary>
        public int product_id { get; set; }
        /// <summary>
        /// 产品编号
        /// </summary>
        public string sno { get; set; }
        /// <summary>
        /// 产品名称
        /// </summary>
        public string name { get; set; }

        public int IsFinished { get; set; }

        public IList<V_ProductTree_Part> Parts { get; set; }
    }
}
