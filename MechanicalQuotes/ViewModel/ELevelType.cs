﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.ViewModel
{
    /// <summary>
    /// 用户级别
    /// </summary>
    public enum ELevelType
    {
        /// <summary>
        /// 操作员
        /// </summary>
        Operator = 1,
        /// <summary>
        /// 分管理员
        /// </summary>
        Manager  =  2,
        /// <summary>
        /// 管理员
        /// </summary>
        Admin  = 3
    }
}
