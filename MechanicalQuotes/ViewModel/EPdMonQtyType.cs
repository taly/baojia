﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.ViewModel
{
    public enum EPdMonQtyType
    {
        /// <summary>
        /// 10件以下
        /// </summary>
        Under10 = 1,
        /// <summary>
        /// 10-50
        /// </summary>
        Between10To50 = 2,
        Between50And150 = 3,
        Over150 = 4
    }
}
