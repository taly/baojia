﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.ViewModel
{
    public class V_PartMaterial
    {
        /// <summary>
        /// 零件材料编号
        /// </summary>


        public int ID { get; set; }
        /// <summary>
        /// 零件编号
        /// </summary>
        public string Part_sno { get; set; }
        /// <summary>
        /// 零件名
        /// </summary>
        public string Part_name { get; set; }
        /// <summary>
        /// 材料名
        /// </summary>
        public string Raw_name { get; set; }
        /// <summary>
        /// 材料规格
        /// </summary>
        public string Raw_guige { get; set; }
        /// <summary>
        /// 下单数量
        /// </summary>
        public int Qty { get; set; }
        /// <summary>
        /// 单价
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 总重量
        /// </summary>
        public double TotalWeight { get; set; }
        /// <summary>
        /// 金额
        /// </summary>
        public decimal TotalPrice { get; set; }

    }
}
