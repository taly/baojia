﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.ViewModel
{
    /// <summary>
    /// 材料成本占比
    /// </summary>
    public enum EMatRatioType
    {
        UnderPer30 = 1,
        Between30To50Per = 2,
        Between50And65Per = 3,
        Over65Per = 4
    }
}
