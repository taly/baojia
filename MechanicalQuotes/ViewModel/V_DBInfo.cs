﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.ViewModel
{
    public class V_DBInfo
    {
        public string Host { get; set; }
        public string Port { get; set; }

        public string UserID { get; set; }

        public string Password { get; set; }
    }
}
