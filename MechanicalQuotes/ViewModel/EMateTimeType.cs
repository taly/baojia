﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.ViewModel
{
    /// <summary>
    /// 材料与工时成本合计
    /// </summary>
    public enum EMateTimeType
    {
        /// <summary>
        /// 1000元以下
        /// </summary>
        Under1000 = 1,
        /// <summary>
        /// 1000元-5000元
        /// </summary>
        Between1000And5000 = 2,
        /// <summary>
        /// 5000元-10000元
        /// </summary>
        Between5000And10000 = 3,
        /// <summary>
        /// 10000元以上
        /// </summary>
        Over10000 = 4
    }
}
