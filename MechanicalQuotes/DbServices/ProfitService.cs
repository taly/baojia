﻿using com.tanlingyun.baojia.Entity;
using com.tanlingyun.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.DbServices
{
    public class ProfitService:BaseService
    {
        public IList<ProfitSetting> AllProfitSetting(int user_id,int manager_id = 0)
        {
            IList<ProfitSetting> profitlists = new List<ProfitSetting>();
            UserService _userService = new UserService();
            User entityUser = _userService.GetUser(user_id);
            if (entityUser == null || entityUser.status != 1)
                return profitlists;
            if (entityUser.level_type == (int)ELevelType.Operator)
                return profitlists;
            else if (entityUser.level_type == (int)ELevelType.Manager)
                manager_id = entityUser.id;

            profitlists = db.Sql("select * from ProfitSetting where manager_id=@0", manager_id).QueryMany<ProfitSetting>();
            return profitlists;
        }

        public ProfitSetting GetSetting(EMateTimeType ettype, EPdMonQtyType mqtype, EMatRatioType mrtype, int manager_id = 0)
        {
            return db.Sql("select * from ProfitSetting where MateTimeType=@0 and PdMonQtyType=@1 and MatRatioType=@2 and manager_id=@3", (int)ettype, (int)mqtype, (int)mrtype,manager_id).QuerySingle<ProfitSetting>();
        }

        public OperateResult SaveProfitSetting(int user_id,ProfitSetting setting)
        {
            if (setting.Profit > 1)
                return new OperateResult() { code = ResultCode.Failed,message  = "利润值处于0-1之间，最多4位小数！" };

            UserService _userService = new UserService();
            User entityUser = _userService.GetUser(user_id);
            if (entityUser == null || entityUser.status != 1)
                return new OperateResult() { code = ResultCode.Failed,message  = "用户信息有误，请重新登录！" };

            if (entityUser.level_type == (int)ELevelType.Operator)
                return new OperateResult() { code = ResultCode.Failed, message = "操作员无法进行此操作！" };
            else if (entityUser.level_type == (int)ELevelType.Manager)
                setting.manager_id = entityUser.id;
            else
                setting.manager_id = 0;

            ProfitSetting entitySet = GetSetting((EMateTimeType)setting.MateTimeType, (EPdMonQtyType)setting.PdMonQtyType, (EMatRatioType)setting.MateTimeType, setting.manager_id);
            if (entitySet != null && setting.ID != entitySet.ID)
                setting.ID = entitySet.ID;

            setting.Profit = Math.Round(setting.Profit, 4);

            SetProfitSetingRange(ref setting);

            if (setting.ID == 0)
            {
                if (db.Insert<ProfitSetting>("ProfitSetting", setting).AutoMap(x => x.ID).Execute() > 0)
                    return new OperateResult() { code = ResultCode.Success };
            }else
            {
                if (db.Update<ProfitSetting>("ProfitSetting", setting).AutoMap(x => x.ID, x => x.manager_id).Where(x => x.ID).Execute() > 0)
                    return new OperateResult() { code = ResultCode.Success };
            }
            return new OperateResult() { code = ResultCode.Failed,message = "数据保存失败！" };
        }

        public void SetProfitSetingRange(ref ProfitSetting set)
        {
            switch (set.MateTimeType)
            {
                default:
                case (int)EMateTimeType.Under1000:
                    set.MateTimeFr = 0;
                    set.MateTimeTo = 1000;
                    break;
                case (int)EMateTimeType.Between1000And5000:
                    set.MateTimeFr = 1000;
                    set.MateTimeTo = 5000;
                    break;
                case (int)EMateTimeType.Between5000And10000:
                    set.MateTimeFr = 5000;
                    set.MateTimeTo = 10000;
                    break;
                case (int)EMateTimeType.Over10000:
                    set.MateTimeFr = 10000;
                    set.MateTimeTo = 100000000;
                    break;
            }
            switch (set.PdMonQtyType)
            {
                case (int)EPdMonQtyType.Under10:
                    set.PdMonQtyFr = 0;
                    set.PdMonQtyTo = 10;
                    break;
                case (int)EPdMonQtyType.Between10To50:
                    set.PdMonQtyFr = 10;
                    set.PdMonQtyTo = 50;
                    break;
                case (int)EPdMonQtyType.Between50And150:
                    set.PdMonQtyFr = 50;
                    set.PdMonQtyTo = 150;
                    break;
                case (int)EPdMonQtyType.Over150:
                    set.PdMonQtyFr = 150;
                    set.PdMonQtyTo = 100000000;
                    break;
                default:
                    break;
            }
            switch (set.MatRatioType)
            {
                case (int)EMatRatioType.UnderPer30:
                    set.MatRatioFr = 0;
                    set.MatRatioTo = 0.3F;
                    break;
                case (int)EMatRatioType.Between30To50Per:
                    set.MatRatioFr = 0.3F;
                    set.MatRatioTo = 0.5F;
                    break;
                case (int)EMatRatioType.Between50And65Per:
                    set.MatRatioFr = 0.5F;
                    set.MatRatioTo = 0.65F;
                    break;
                case (int)EMatRatioType.Over65Per:
                    set.MatRatioFr = 0.65F;
                    set.MatRatioTo = 1;
                    break;
                default:
                    break;
            }
        }
    }


}
