﻿using com.tanlingyun.baojia.Entity;
using com.tanlingyun.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.DbServices
{
    public class GongxuService:BaseService
    {
        /// <summary>
        /// 获取工序分页
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="search"></param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        public PageInfo<Gongxu> GetPageList(int user_id, string search = "",int PageIndex = 1,int PageSize = 10,int manager_id = 0)
        {
            PageInfo<Gongxu> gxPage = new PageInfo<Gongxu>() {
                PageIndex = PageIndex,
                PageSize = PageSize,
                Fields  = "*",
                TableName = "Gongxu",
                OrderBy = "ID desc"
            };

            UserService _userService = new UserService();
            User entityUser = _userService.GetUser(user_id);
            if (entityUser == null || entityUser.status != 1 || entityUser.isgrant != 1)
                return gxPage;

            string where = "status=1 ";
            if (entityUser.level_type == (int)ELevelType.Operator)
                where += " and manager_id=" + entityUser.parentid;
            else if (entityUser.level_type == (int)ELevelType.Admin)
                where += " and manager_id=" + manager_id;
            else
                where += " and manager_id=" + entityUser.id;

            if (!string.IsNullOrWhiteSpace(search))
                where += " and (name like '%" + search + "%' or alias like '%" + search + "%')";
            gxPage.Where = where;
            return GetPageInfo<Gongxu>(gxPage);
        }

        /// <summary>
        /// 添加工序
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="gx"></param>
        /// <returns></returns>
        public OperateResult AddGongxu(int user_id,Gongxu gx)
        {
            gx.status = 1;

            UserService _userService = new UserService();
            User entityUser = _userService.GetUser(user_id);
            if (entityUser == null || entityUser.status != 1 || entityUser.isgrant != 1)
                return new OperateResult() { code = ResultCode.Failed, message = "用户信息错误，请重新登录！" };
            if (entityUser.level_type == (int)ELevelType.Operator)
                return new OperateResult() { code = ResultCode.Failed, message = "无权限！" };
            else if (entityUser.level_type == (int)ELevelType.Admin)
                gx.manager_id = 0;
            else if (entityUser.level_type == (int)ELevelType.Manager)
                gx.manager_id = entityUser.id;

            if (db.Insert<Gongxu>("Gongxu", gx).AutoMap(x => x.ID).Execute() > 0)
                return new OperateResult() { code = ResultCode.Success };
  
            return new OperateResult() { code = ResultCode.Failed,message = "工序添加失败！" };
        }

        /// <summary>
        /// 更新工序
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="gx"></param>
        /// <returns></returns>
        public OperateResult UpdateGx(int user_id, Gongxu gx)
        {
            Gongxu entituGx = GetGx(gx.ID);
            if (entituGx == null || entituGx.status != 1)
                return new OperateResult() { code = ResultCode.Failed,message ="更新的工序不存在！" };

            //验证用户身份
            UserService _userService = new UserService();
            User entityUser = _userService.GetUser(user_id);
            if (entityUser == null || entityUser.status != 1 || entityUser.isgrant != 1)
                return new OperateResult() { code = ResultCode.Failed, message = "用户信息有误，请重新登录！" };

            if (entityUser.level_type == (int)ELevelType.Operator)
                return new OperateResult() { code = ResultCode.Failed, message = "无权限!" };
            else if (entityUser.level_type == (int)ELevelType.Manager && entituGx.manager_id != entityUser.id)
                return new OperateResult() { code = ResultCode.Failed, message = "无法更新非分管理员的数据！" };

            gx.ID = entituGx.ID;
            gx.status = entituGx.status;
            if (db.Update<Gongxu>("Gongxu", gx).AutoMap(x => x.ID,x=>x.manager_id,x=>x.status).Where(x => x.ID).Execute() > 0)
                return new OperateResult() { code = ResultCode.Success };
            return new OperateResult() { code = ResultCode.Failed,message = "工序更新失败！" };
        }

        /// <summary>
        /// 删除工序
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="gx_id"></param>
        /// <returns></returns>
        public OperateResult DeleteGx(int user_id, int gx_id)
        {
            Gongxu entityGx = GetGx(gx_id);
            if (entityGx == null || entityGx.status != 1)
                return new OperateResult() { code = ResultCode.Failed, message = "删除的工序不存在!" };

            UserService _userService = new UserService();
            User entityUser = _userService.GetUser(user_id);
            if (entityUser == null || entityUser.status != 1 || entityUser.isgrant != 1)
                return new OperateResult() { code = ResultCode.Failed, message = "用户信息有误，请重新登录！" };

            if (entityUser.level_type == (int)ELevelType.Operator)
                return new OperateResult() { code = ResultCode.Failed, message = "无权限!" };
            else if (entityUser.level_type == (int)ELevelType.Manager && entityGx.manager_id != entityUser.id)
                return new OperateResult() { code = ResultCode.Failed, message = "无法删除非分管理员的数据！" };
            
            entityGx.status = 0;
            if (db.Update<Gongxu>("Gongxu", entityGx).Column(x => x.status).Where(x => x.ID).Execute() > 0)
                return new OperateResult() { code = ResultCode.Success };
            return new OperateResult() { code = ResultCode.Failed, message = "删除工序失败!" };
        }

        public Gongxu GetGx(int gx_id)
        {
            return db.Sql("select * from Gongxu where ID=@0", gx_id).QuerySingle<Gongxu>();
        }
    }
}
