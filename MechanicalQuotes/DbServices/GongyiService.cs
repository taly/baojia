﻿using com.tanlingyun.baojia.Entity;
using com.tanlingyun.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.DbServices
{
    /// <summary>
    /// 工艺及术语
    /// </summary>
    public class GongyiService:BaseService
    {
        /// <summary>
        /// 获取术语列表
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="search"></param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        public PageInfo<Shuyu> list(int user_id,string search = "",int PageIndex = 1,int PageSize = 10,int manager_id = 0)
        {
            PageInfo<Shuyu> plist = new PageInfo<Shuyu>() {
                PageIndex = PageIndex,
                PageSize = PageSize,
                Fields = "*",
                TableName = "Shuyu",
                OrderBy = "ID desc"
            };

            UserService _userService = new UserService();
            User entityUser = _userService.GetUser(user_id);
            if (entityUser == null || entityUser.status != 1 || entityUser.isgrant != 1)
                return plist;

            string where = "status=1 ";
            if (entityUser.level_type == (int)ELevelType.Operator)
                where += " and manager_id=" + entityUser.parentid;
            else if (entityUser.level_type == (int)ELevelType.Admin)
                where += " and manager_id=" + manager_id;
            else
                where += " and manager_id=" + entityUser.id;

            if (!string.IsNullOrWhiteSpace(search))
                where += " and (name like '%" + search + "%' or alias like '%" + search + "%' or Gyshuyu like '%" + search + "%')";
            plist.Where = where;
            return GetPageInfo<Shuyu>(plist);
        }

        /// <summary>
        /// 添加工艺术语
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="shuyu"></param>
        /// <returns></returns>
        public OperateResult AddShuyu(int user_id,Shuyu shuyu)
        {
            //Shuyu entityShuyu = GetShuyu(shuyu.name, shuyu.Gyshuyu);
            //if (entityShuyu != null && entityShuyu.status == 1)
            //    return new OperateResult() { code = ResultCode.Failed, message = string.Format("工艺名称“{0}”-工艺术语“{1}”已经存在，不可重复添加！", shuyu.name, shuyu.Gyshuyu) };
            shuyu.status = 1;

            UserService _userService = new UserService();
            User entityUser = _userService.GetUser(user_id);
            if (entityUser == null || entityUser.status != 1 || entityUser.isgrant != 1)
                return new OperateResult() { code = ResultCode.Failed, message = "用户信息错误，请重新登录！" };
            if (entityUser.level_type == (int)ELevelType.Operator)
                return new OperateResult() { code = ResultCode.Failed, message = "无权限！" };
            else if (entityUser.level_type == (int)ELevelType.Admin)
                shuyu.manager_id = 0;
            else if (entityUser.level_type == (int)ELevelType.Manager)
                shuyu.manager_id = entityUser.id;

            if (db.Insert("Shuyu", shuyu).AutoMap(x => x.ID).Execute() > 0)
                return new OperateResult() { code = ResultCode.Success };
            return new OperateResult() { code = ResultCode.Failed,message = "术语添加失败!"};
        }

        /// <summary>
        /// 删除术语
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="shuyu_id"></param>
        /// <returns></returns>
        public OperateResult DeleteShuyu(int user_id,int shuyu_id)
        {
            Shuyu entityShuyu = GetShuyu(shuyu_id);
            if (entityShuyu == null || entityShuyu.status != 1)
                return new OperateResult() { code = ResultCode.Failed,message = "术语不存在，无法删除！" };

            UserService _userService = new UserService();
            User entityUser = _userService.GetUser(user_id);
            if (entityUser == null || entityUser.status != 1 || entityUser.isgrant != 1)
                return new OperateResult() { code = ResultCode.Failed, message = "用户信息有误，请重新登录！" };

            if (entityUser.level_type == (int)ELevelType.Operator)
                return new OperateResult() { code = ResultCode.Failed, message = "无权限!" };
            else if (entityUser.level_type == (int)ELevelType.Manager && entityShuyu.manager_id != entityUser.id)
                return new OperateResult() { code = ResultCode.Failed, message = "无法删除非分管理员的数据！" };

            entityShuyu.status = 0;     
            if (db.Update<Shuyu>("Shuyu", entityShuyu).Column(x=>x.status).Where(x => x.ID).Execute() > 0)
                return new OperateResult() { code = ResultCode.Success };
            return new OperateResult() { code = ResultCode.Failed,message = "删除术语失败！" };
        }

        /// <summary>
        /// 更新术语
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="shuyu"></param>
        /// <returns></returns>
        public OperateResult UpdateShuyu(int user_id,Shuyu shuyu)
        {
            Shuyu entityShuyu = GetShuyu(shuyu.ID);
            if (entityShuyu == null || entityShuyu.status != 1)
                return new OperateResult() { code = ResultCode.Failed,message = "术语不存在，无法进行更新！" };

            //验证用户身份
            UserService _userService = new UserService();
            User entityUser = _userService.GetUser(user_id);
            if (entityUser == null || entityUser.status != 1 || entityUser.isgrant != 1)
                return new OperateResult() { code = ResultCode.Failed, message = "用户信息有误，请重新登录！" };

            if (entityUser.level_type == (int)ELevelType.Operator)
                return new OperateResult() { code = ResultCode.Failed, message = "无权限!" };
            else if (entityUser.level_type == (int)ELevelType.Manager && entityShuyu.manager_id != entityUser.id)
                return new OperateResult() { code = ResultCode.Failed, message = "无法更新非分管理员的数据！" };

            entityShuyu.name = shuyu.name;
            entityShuyu.alias = shuyu.alias;
            entityShuyu.Gyshuyu = shuyu.Gyshuyu;

            if (db.Update<Shuyu>("Shuyu", entityShuyu).AutoMap(x => x.ID, x => x.status, x => x.manager_id).Where(x => x.ID).Execute() > 0)
                return new OperateResult() { code = ResultCode.Success };

            return new OperateResult() { code = ResultCode.Failed,message = "术语更新失败!" };
        }

        protected Shuyu GetShuyu(int shuyu_id)
        {
            return db.Sql("select * from Shuyu where ID=@0 order by ID desc",shuyu_id).QuerySingle<Shuyu>();
        }

    }
}
