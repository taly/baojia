﻿using com.tanlingyun.baojia.Entity;
using com.tanlingyun.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.DbServices
{
    /// <summary>
    /// 员工管理
    /// </summary>
    public class EmployeeService:BaseService
    {
        /// <summary>
        /// 获取员工分页
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="search"></param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        public PageInfo<Employee> GetEmployeeList(int user_id, string search = "", int PageIndex = 1, int PageSize = 10,int manager_id = 0)
        {
            PageInfo<Employee> empPage = new PageInfo<Employee>() {
                Fields = "*",
                TableName = "Employee",
                OrderBy = "id desc",
                PageIndex = PageIndex,
                PageSize = PageSize
            };

            UserService _userService = new UserService();
            User entityUser = _userService.GetUser(user_id);
            if (entityUser == null || entityUser.status != 1 || entityUser.isgrant != 1)
                return empPage;

            string where = "status=1 ";
            if (entityUser.level_type == (int)ELevelType.Operator)
                where += " and manager_id=" + entityUser.parentid;
            else if (entityUser.level_type == (int)ELevelType.Admin)
                where += " and manager_id=" + manager_id;
            else
                where += " and manager_id=" + entityUser.id;

            if (!string.IsNullOrWhiteSpace(search))
                where += "name like '%" + search + "%' or alias like '%" + search + "%'";
            empPage.Where = where;
            return GetPageInfo<Employee>(empPage);
        }

        /// <summary>
        /// 添加职员
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="emp"></param>
        /// <returns></returns>
        public OperateResult AddEmployee(int user_id,Employee emp)
        {
            emp.status = 1;

            UserService _userService = new UserService();
            User entityUser = _userService.GetUser(user_id);
            if (entityUser == null || entityUser.status != 1 || entityUser.isgrant != 1)
                return new OperateResult() { code = ResultCode.Failed, message = "用户信息错误，请重新登录！" };
            if (entityUser.level_type == (int)ELevelType.Operator)
                return new OperateResult() { code = ResultCode.Failed, message = "无权限！" };
            else if (entityUser.level_type == (int)ELevelType.Admin)
                emp.manager_id = 0;
            else if (entityUser.level_type == (int)ELevelType.Manager)
                emp.manager_id = entityUser.id;

            if (db.Insert<Employee>("Employee", emp).AutoMap(x => x.id).Execute() > 0)
                return new OperateResult() { code = ResultCode.Success };
            return new OperateResult() { code = ResultCode.Failed ,message = "添加失败!" };
        }

        /// <summary>
        /// 删除员工
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="employee_id"></param>
        /// <returns></returns>
        public OperateResult DeleteEmployee(int user_id,int employee_id)
        {
            Employee emp = GetEmployee(employee_id);
            if (emp == null || emp.status != 1)
                return new OperateResult() { code = ResultCode.Failed, message = "员工不存在!" };

            UserService _userService = new UserService();
            User entityUser = _userService.GetUser(user_id);
            if (entityUser == null || entityUser.status != 1 || entityUser.isgrant != 1)
                return new OperateResult() { code = ResultCode.Failed, message = "用户信息有误，请重新登录！" };

            if (entityUser.level_type == (int)ELevelType.Operator)
                return new OperateResult() { code = ResultCode.Failed, message = "无权限!" };
            else if (entityUser.level_type == (int)ELevelType.Manager && emp.manager_id != entityUser.id)
                return new OperateResult() { code = ResultCode.Failed, message = "无法删除非分管理员的数据！" };

            emp.status = 0;
            if (db.Update<Employee>("Employee", emp).Column(x => x.status).Where(x => x.id).Execute() > 0)
                return new OperateResult() { code = ResultCode.Success };
            return new OperateResult() { code = ResultCode.Failed,message = "删除失败!" };
        }

        /// <summary>
        /// 更新员工信息
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="emp"></param>
        /// <returns></returns>
        public OperateResult UpdateEmployee(int user_id,Employee emp)
        {
            Employee entityEmp = GetEmployee(emp.id);
            if (entityEmp == null || entityEmp.status != 1)
                return new OperateResult() { code = ResultCode.Failed, message = "员工不存在，无法进行更新！" };

            //验证用户身份
            UserService _userService = new UserService();
            User entityUser = _userService.GetUser(user_id);
            if (entityUser == null || entityUser.status != 1 || entityUser.isgrant != 1)
                return new OperateResult() { code = ResultCode.Failed, message = "用户信息有误，请重新登录！" };

            if (entityUser.level_type == (int)ELevelType.Operator)
                return new OperateResult() { code = ResultCode.Failed, message = "无权限!" };
            else if (entityUser.level_type == (int)ELevelType.Manager && entityEmp.manager_id != entityUser.id)
                return new OperateResult() { code = ResultCode.Failed, message = "无法更新非分管理员的数据！" };

            if (db.Update<Employee>("Employee", emp).AutoMap(x => x.id, x => x.status, x => x.manager_id).Where(x => x.id).Execute() > 0)
                return new OperateResult() { code = ResultCode.Success };
            return new OperateResult() { code = ResultCode.Failed,message = "更新失败!" };
        }

        /// <summary>
        /// 获取职员
        /// </summary>
        /// <param name="employee_id"></param>
        /// <returns></returns>
        public Employee GetEmployee(int employee_id)
        {
            return db.Sql("select * from Employee where id=@0", employee_id).QuerySingle<Employee>();
        }
    }
}
