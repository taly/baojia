﻿using com.tanlingyun.baojia.Entity;
using com.tanlingyun.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.DbServices
{
    public class PriceService:BaseService
    {
        /// <summary>
        /// 添加调差记录
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="rd"></param>
        /// <returns></returns>
        public OperateResult AddPriceRecord(int user_id,PriceRecords rd)
        {
            ProductService _productService = new ProductService();
            Products entityProduct = _productService.GetProduct(rd.product_id);
            if (entityProduct == null || entityProduct.Status != 1)
                return new OperateResult() { code = ResultCode.Failed,message = "产品不存在！" };

            UserService _userService = new UserService();
            User entityUser = _userService.GetUser(user_id);
            if (entityUser == null || entityUser.status != 1)
                return new OperateResult() { code = ResultCode.Failed,message = "用户信息有误，请重新登录！" };

            if (entityUser.level_type == (int)ELevelType.Operator && entityProduct.manager_id != entityUser.parentid)
                return new OperateResult() { code = ResultCode.Failed,message = "无权限，无法操作！" };
            if (entityUser.level_type == (int)ELevelType.Manager && entityProduct.manager_id != entityUser.id)
                return new OperateResult() { code = ResultCode.Failed, message = "无权限，无法操作！" };

            rd.operator_user = user_id;
            rd.operator_time = DateTime.Now;

            if (db.Insert<PriceRecords>("PriceRecords", rd).AutoMap(x => x.ID).Execute() > 0)
                return new OperateResult() { code = ResultCode.Success };
            return new OperateResult() { code = ResultCode.Failed,message = "添加产品价格记录失败！" };
        }

        /// <summary>
        /// 获取价格历史记录
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="search"></param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        public PageInfo<V_list_pricerecords> GetPriceRecord(int user_id,string search = "",int PageIndex = 1,int PageSize = 10)
        {
            PageInfo<V_list_pricerecords> prdpage = new PageInfo<V_list_pricerecords>() {
                PageIndex = PageIndex,
                PageSize = PageSize,
                Fields = "pricerecords.ID as pr_id,products.Name,products.price,pricerecords.operator_time as update_time,user.name as update_user ",
                TableName = "pricerecords left join products on pricerecords.product_id = products.ID left join user on pricerecords.operator_user = user.id",
                OrderBy = "pricerecords.ID desc"
            };
            string where = "1=1 ";

            UserService _userService = new UserService();
            User entityUser = _userService.GetUser(user_id);
            if (entityUser == null || entityUser.status != 1)
                return prdpage;
            if (entityUser.level_type == (int)ELevelType.Operator)
                return prdpage;
            if (entityUser.level_type == (int)ELevelType.Manager)
                where += " and products.manage_id=" + entityUser.id;
            prdpage.Where = where;
            return GetPageInfo<V_list_pricerecords>(prdpage);
        }
    }
}
