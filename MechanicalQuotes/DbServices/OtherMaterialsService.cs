﻿using com.tanlingyun.baojia.Entity;
using com.tanlingyun.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.DbServices
{
    /// <summary>
    /// 其他材料（外购件，标准件，辅助材料）
    /// </summary>
    public class OtherMaterialsService:BaseService
    {
        public PageInfo<OtherRm> GetOtherRmPage(int user_id,string search = "",int PageIndex = 1,int PageSize = 10,int manager_id = 0)
        {
            PageInfo<OtherRm> orPage = new PageInfo<OtherRm>() {
                Fields = "*",
                TableName = "OtherRm",
                PageIndex = PageIndex,
                PageSize = PageSize,
                OrderBy = "ID desc"
            };

            UserService _userService = new UserService();
            User entityUser = _userService.GetUser(user_id);
            if (entityUser == null || entityUser.status != 1 || entityUser.isgrant != 1)
                return orPage;

            string where = "status=1 ";
            if (entityUser.level_type == (int)ELevelType.Operator)
                where += " and manager_id=" + entityUser.parentid;
            else if (entityUser.level_type == (int)ELevelType.Admin)
                where += " and manager_id=" + manager_id;
            else
                where += " and manager_id=" + entityUser.id;

            if (!string.IsNullOrWhiteSpace(search))
                where += " and (name like '%" + search + "%' or SNO like '%"+search+"%' or alias like '%" + search + "%')";
            orPage.Where = where;
            return GetPageInfo<OtherRm>(orPage);
        }

        /// <summary>
        /// 添加物料
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="rm"></param>
        /// <returns></returns>
        public OperateResult AddOther(int user_id,OtherRm rm)
        {
            //OtherRm entityRm = GetOtherBySno(rm.SNO);
            //if (entityRm != null && entityRm.status == 1)
            //    return new OperateResult() { code = ResultCode.Failed,message = string.Format("物料编号{0}已经存在，不可以重复添加！",rm.SNO)};

            rm.status = 1;

            UserService _userService = new UserService();
            User entityUser = _userService.GetUser(user_id);
            if (entityUser == null || entityUser.status != 1 || entityUser.isgrant != 1)
                return new OperateResult() { code = ResultCode.Failed, message = "用户信息错误，请重新登录！" };
            if (entityUser.level_type == (int)ELevelType.Operator)
                return new OperateResult() { code = ResultCode.Failed, message = "无权限！" };
            else if (entityUser.level_type == (int)ELevelType.Admin)
                rm.manager_id = 0;
            else if (entityUser.level_type == (int)ELevelType.Manager)
                rm.manager_id = entityUser.id;

            if (db.Insert<OtherRm>("OtherRm", rm).AutoMap(x => x.ID).Execute() > 0)
                return new OperateResult() { code = ResultCode.Success };
            return new OperateResult() { code = ResultCode.Failed,message = "添加失败!" };
        }

        /// <summary>
        /// 更新物料
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="rm"></param>
        /// <returns></returns>
        public OperateResult UpdateOther(int user_id,OtherRm rm)
        {
            OtherRm entityRm = GetOtherRm(rm.ID);
            if (entityRm == null || entityRm.status != 1)
                return new OperateResult() { code = ResultCode.Failed,message = "更新的物料不存在" };

            //验证用户身份
            UserService _userService = new UserService();
            User entityUser = _userService.GetUser(user_id);
            if (entityUser == null || entityUser.status != 1 || entityUser.isgrant != 1)
                return new OperateResult() { code = ResultCode.Failed, message = "用户信息有误，请重新登录！" };

            if (entityUser.level_type == (int)ELevelType.Operator)
                return new OperateResult() { code = ResultCode.Failed, message = "无权限!" };
            else if (entityUser.level_type == (int)ELevelType.Manager && entityRm.manager_id != entityUser.id)
                return new OperateResult() { code = ResultCode.Failed, message = "无法更新非分管理员的数据！" };

            rm.ID = entityRm.ID;
            rm.status = entityRm.status;
            if (db.Update<OtherRm>("OtherRm", rm).AutoMap(x => x.ID, x => x.status, x => x.manager_id).Where(x => x.ID).Execute() > 0)
                return new OperateResult() { code = ResultCode.Success };
            return new OperateResult() { code = ResultCode.Failed,message = "更新失败!" };
        }

        /// <summary>
        /// 删除物料
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="or_id"></param>
        /// <returns></returns>
        public OperateResult DelteItem(int user_id, int or_id)
        {
            OtherRm entityOther = GetOtherRm(or_id);
            if (entityOther == null || entityOther.status != 1)
                return new OperateResult() { code = ResultCode.Failed, message = "删除的物料不存在!" };

            UserService _userService = new UserService();
            User entityUser = _userService.GetUser(user_id);
            if (entityUser == null || entityUser.status != 1 || entityUser.isgrant != 1)
                return new OperateResult() { code = ResultCode.Failed, message = "用户信息有误，请重新登录！" };

            if (entityUser.level_type == (int)ELevelType.Operator)
                return new OperateResult() { code = ResultCode.Failed, message = "无权限!" };
            else if (entityUser.level_type == (int)ELevelType.Manager && entityOther.manager_id != entityUser.id)
                return new OperateResult() { code = ResultCode.Failed, message = "无法删除非分管理员的数据！" };

            entityOther.status = 0;
            if (db.Update<OtherRm>("OtherRm", entityOther).Column(x => x.status).Where(x => x.ID).Execute() > 0)
                return new OperateResult() { code = ResultCode.Success };
            return new OperateResult() { code = ResultCode.Failed,message = "删除失败!"};
        }


        protected OtherRm GetOtherRm(int other_id)
        {
            return db.Sql("select * from OtherRm where ID=@0",other_id).QuerySingle<OtherRm>();
        }
    }
}
