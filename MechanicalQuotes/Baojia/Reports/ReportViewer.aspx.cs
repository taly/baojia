﻿using com.tanlingyun.baojia.Entity;
using com.tanlingyun.DbServices;
using com.tanlingyun.helper;
using com.tanlingyun.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.tanlingyun.Baojia.Reports
{
    public partial class ReportViewer : System.Web.UI.Page
    {
        public User entityUser = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                GetUser();
                if (entityUser == null)
                    return;
                string type = Request.QueryString["type"];
                string productid = Request.QueryString["productid"];
                ProductService _productService = new ProductService();
                IList<V_PartMaterial> pmlist = _productService.GetPartMaterial(0, Convert.ToInt32(productid));
                DataTable table = new DataTable();
                table.Columns.AddRange(new DataColumn[] {
                        new DataColumn("ID",typeof(int)),
                        new DataColumn("Part_sno",typeof(string)),
                        new DataColumn("Part_name",typeof(string)),
                        new DataColumn("Raw_name",typeof(string)),
                        new DataColumn("Raw_guige",typeof(string)),
                        new DataColumn("Qty",typeof(int)),
                        new DataColumn("Price",typeof(decimal)),
                        new DataColumn("TotalWeight",typeof(double)),
                        new DataColumn("TotalPrice",typeof(decimal))
                    });
                foreach (var item in pmlist)
                {
                    table.Rows.Add(item.ID, item.Part_sno, item.Part_name, item.Raw_name, item.Raw_guige, item.Qty, Math.Round(item.Price,2), Math.Round(item.TotalWeight,3), Math.Round(item.TotalPrice,2));
                }

                DataTable product_table = new DataTable();
                product_table.Columns.AddRange(new DataColumn[] {
                    new DataColumn("Sno",typeof(string)),
                    new DataColumn("Name",typeof(string)),
                    new DataColumn("Customer",typeof(string)),
                    new DataColumn("Operator",typeof(string))
                });
                Products pd = _productService.GetProduct(int.Parse(productid));
                product_table.Rows.Add(pd.Sno, pd.Name, pd.Customer, entityUser.name);

                if (type == "1")
                {
                    this.ReportViewer1.LocalReport.ReportPath = Server.MapPath("/") + "Reports/cailiaoqingdan.rdlc";                    
                }else if(type == "2")
                {
                    this.ReportViewer1.LocalReport.ReportPath = Server.MapPath("/") + "Reports/cailiaohuizong.rdlc";
                }
                this.ReportViewer1.LocalReport.DataSources.Clear();
                this.ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", table));
                this.ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet2", product_table));

            }
        }

        private void GetUser()
        {
            string userCookie = CookieHelper.GetValue(".user") as string;
            if (!string.IsNullOrWhiteSpace(userCookie))
            {
                userCookie = com.tanlingyun.helper.UtilityHelper.DecodeBase64(userCookie);
                string[] userArr = userCookie.Split(':');
                if (userArr != null && userArr.Length == 2)
                {
                    UserService _userService = new UserService();
                    OperateResult<User> result = _userService.ValidateUser(int.Parse(userArr[0]), userArr[1]);
                    if (result.code == ResultCode.Success)
                    {
                        entityUser = result.data;
                    }
                }
            }
        }
    }
}