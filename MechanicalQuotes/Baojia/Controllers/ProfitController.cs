﻿using com.tanlingyun.baojia.Entity;
using com.tanlingyun.DbServices;
using com.tanlingyun.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.tanlingyun.Baojia.Controllers
{
    /// <summary>
    /// 利润管理
    /// </summary>
    public class ProfitController : BaseController
    {
        /// <summary>
        /// 利润管理
        /// </summary>
        /// <returns></returns>
        [UserType(type = new ViewModel.ELevelType[] { ViewModel.ELevelType.Admin, ViewModel.ELevelType.Manager })]
        public ActionResult list(int manager_id = 0)
        {
            ProfitService _profitService = new ProfitService();
            IList<ProfitSetting> allset = _profitService.AllProfitSetting(entityUser.id, manager_id);
            IList<ProfitSetting> alllist = new List<ProfitSetting>();
            for (int i = 1; i <= 4; i++)
            {
                for (int k = 1; k <= 4; k++)
                {
                    for (int m = 1; m <= 4; m++)
                    {
                        ProfitSetting set = allset.Where(x => x.MateTimeType == i && x.PdMonQtyType == k && x.MatRatioType == m).FirstOrDefault();
                        if(set == null)
                        {
                            set = new ProfitSetting() {
                                MatRatioType = m,
                                PdMonQtyType = k,
                                MateTimeType = i,
                                manager_id = manager_id
                            };
                        }else
                        {
                            set = set;
                        }
                        alllist.Add(set);
                    }
                }
            }

            return View(alllist);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="p3"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public JsonResult Modify(int p1,int p2,int p3,double value)
        {
            ProfitService _profitService = new ProfitService();
            OperateResult result = _profitService.SaveProfitSetting(entityUser.id, new ProfitSetting() { MateTimeType = p1, PdMonQtyType = p2, MatRatioType = p3, Profit = value });
            if (result.code == ResultCode.Success)
                return Json(new { success = true });
            return Json(new { success = false, msg = result.message });
        }
    }
}