﻿using com.tanlingyun.baojia.Entity;
using com.tanlingyun.DbServices;
using com.tanlingyun.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.tanlingyun.Baojia.Controllers
{
    /// <summary>
    /// 系统管理
    /// </summary>
    public class SystemController : BaseController
    {
        // GET: System
        public ActionResult Index()
        {
            return View();
        }

        [UserType(type = new ViewModel.ELevelType[] { ViewModel.ELevelType.Admin,ViewModel.ELevelType.Manager })]
        public ActionResult InitUserData()
        {

            return View();
        }

        /// <summary>
        /// 数据备份和恢复
        /// </summary>
        /// <returns></returns>
        [UserType(type = new ViewModel.ELevelType[] { ViewModel.ELevelType.Admin })]
        public ActionResult Backup()
        {

            return View();
        }

        /// <summary>
        /// 分页
        /// </summary>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <param name="search"></param>
        /// <returns></returns>
        [HttpPost]
        [UserType(type = new ViewModel.ELevelType[] { ELevelType.Admin })]
        public JsonResult recordlist(int PageIndex = 1, int PageSize = 10)
        {
            SysService _sysService = new SysService();
            PageInfo<BackupRecords> rpage = _sysService.Backuplist(entityUser.id, PageIndex, PageSize);
            return DataPageResult<BackupRecords>(rpage);
        }

        /// <summary>
        /// 添加备份
        /// </summary>
        /// <param name="bkname"></param>
        /// <returns></returns>
        [HttpPost]
        [UserType(type = new ViewModel.ELevelType[] { ELevelType.Admin })]
        public JsonResult AddBackup(string bkname = "")
        {
            if(string.IsNullOrWhiteSpace(bkname) || bkname.Length > 50)
            {
                return ErrorResult(ResultCode.Failed,"备份名不能为空且长度不能超过50个字符！");
            }
            SysService _sysService = new SysService();
           
            OperateResult result = _sysService.CreateBackupPoint(entityUser.id, new BackupRecords() { Backup_user = entityUser.id, Name = bkname,Status = 1 });
            if (result.code == ResultCode.Success)
                return SuccessResult();
            return ErrorResult(result.code, result.message);
        }

        /// <summary>
        /// 还原备份
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [UserType(type = new ViewModel.ELevelType[] { ELevelType.Admin })]
        public JsonResult RestoreBackup(int id = 0)
        {
            SysService _sysService = new SysService();
            OperateResult result =  _sysService.RestoreBackup(entityUser.id, id);
            if (result.code == ResultCode.Success)
                return SuccessResult();
            return ErrorResult(result.code, result.message);
        }

        /// <summary>
        /// 删除备份
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [UserType(type = new ViewModel.ELevelType[] { ELevelType.Admin })]
        public JsonResult DeleteBackup(int id = 0)
        {
            if (id == 0)
                return ErrorResult(ResultCode.Failed,"未提供删除对象！");
            SysService _sysServie = new SysService();
            OperateResult result = _sysServie.DeleteBackup(entityUser.id, id);
            return ErrorResult(result.code, result.message);
        }

        /// <summary>
        /// 初始化操作员数据
        /// </summary>
        /// <param name="user_id"></param>
        /// <returns></returns>
        public JsonResult InitData(int user_id)
        {
            SysService _sysService = new SysService();
            OperateResult result = _sysService.InitOperatorData(entityUser.id, user_id);
            if (result.code == ResultCode.Success)
                return SuccessResult();
            return ErrorResult(result.code, result.message);
        }
    }
}