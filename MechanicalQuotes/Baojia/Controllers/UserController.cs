﻿using com.tanlingyun.baojia.Entity;
using com.tanlingyun.DbServices;
using com.tanlingyun.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.tanlingyun.Baojia.Controllers
{
    [RoutePrefix("user")]
    public class UserController : BaseController
    {
        [AllowAnonymous]
        public ActionResult SignIn()
        {
            return View();
        }

        /// <summary>
        /// 验证登录
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public JsonResult CheckLogin(string username,string password)
        {
            UserService _userServices = new UserService();
            OperateResult<User> result = _userServices.CheckUser(username, password);
            if (result.code != ResultCode.Success)
                return ErrorResult(result.code, result.message);
            else
                return SuccessResult();
        }

        /// <summary>
        /// 验证注册
        /// </summary>
        /// <param name="username"></param>
        /// <param name="name"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public JsonResult CheckRegister(string username,string name,string password)
        {
            if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(password))
                return ErrorResult(ResultCode.Failed,"不能为空!");
            UserService _userService = new UserService();
            OperateResult result = _userService.Register(username, password, name);
            if (result.code == ResultCode.Success)
                return SuccessResult();
            else
                return ErrorResult(result.code, result.message);
        }

        /// <summary>
        /// 用户管理
        /// </summary>
        /// <returns></returns>
        [UserType(type = new ViewModel.ELevelType[] { ELevelType.Admin,ELevelType.Manager,ELevelType.Operator })]
        public ActionResult Index()
        {

            return View();
        }

        /// <summary>
        /// 管理员管理页面
        /// </summary>
        /// <returns></returns>
        [UserType(type = new ViewModel.ELevelType[] { ELevelType.Admin })]
        public ActionResult ManageIndex()
        {
            return View();
        }

        /// <summary>
        /// 操作员管理页面
        /// </summary>
        /// <returns></returns>
        [UserType(type = new ViewModel.ELevelType[] { ELevelType.Admin,ELevelType.Manager })]
        public ActionResult OperatorIndex()
        {
            return View();
        }

        [UserType(type = new ViewModel.ELevelType[] { ELevelType.Admin, ELevelType.Manager })]
        public JsonResult userlist(int PageIndex = 1, int PageSize = 10, string search = "",int type = (int)ELevelType.Operator)
        {
            UserService _userService = new UserService();
            PageInfo<V_list_user> upage = _userService.GetUserPage(entityUser.id, search, PageIndex, PageSize, (ELevelType)type);
            return DataPageResult<V_list_user>(upage);
        }

        /// <summary>
        /// 获取所有管理员
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult allmanager()
        {
            UserService _userService = new UserService();
            if (entityUser.level_type != (int)ELevelType.Admin)
                return ErrorResult(ResultCode.NoPermission);
            PageInfo<V_list_user> userlist = _userService.GetUserPage(entityUser.id, "", 1, 9999, ELevelType.Manager);
            return Json(new { code = (int)ResultCode.Success, data = userlist.List.Select(x => new { x.name, x.id }) });
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="upd_user"></param>
        /// <param name="userpwd"></param>
        /// <param name="newPwd"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ChangePassword(int upd_user,string curpwd, string newPwd)
        {
            if (upd_user == 0)
                return ErrorResult(ResultCode.Failed,"未选择更改的用户！");
            if (string.IsNullOrWhiteSpace(curpwd) || string.IsNullOrWhiteSpace(newPwd))
                return ErrorResult(ResultCode.Failed,"密码不能为空！");
            if (newPwd.Length < 6 || newPwd.Length > 10)
                return ErrorResult(ResultCode.Failed,"新密码长度必须6-10位！");
            UserService _userService = new UserService();
            OperateResult result = _userService.ChangePassword(entityUser.id, upd_user, newPwd, curpwd);
            if (result.code == ResultCode.Success)
                return SuccessResult();
            else
                return ErrorResult(result.code, result.message);
        }

        /// <summary>
        /// 更改账号授权
        /// </summary>
        /// <param name="grant_user"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpdateGrant(int grant_user,int type = 1)
        {
            UserService _userService = new UserService();
            OperateResult result =  _userService.GrantUser(entityUser.id, grant_user, type);
            if (result.code == ResultCode.Success)
                return SuccessResult();
            else
                return ErrorResult(result.code, result.message);
        }

        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [Route("adddata")]
        [HttpPost]
        public JsonResult AddData(User data)
        {
            if (data == null)
                return ErrorResult(ResultCode.Failed, "数据为空!");
            //验证数据格式
            if (!ModelState.IsValid)
                return ErrorResult(ResultCode.Failed, ModelState.Where(x => x.Value.Errors.Count > 0).FirstOrDefault().Value.Errors.FirstOrDefault().ErrorMessage);

            UserService _userService = new UserService();

            OperateResult result = null;
            if (data.id == 0)
            {
                if (string.IsNullOrWhiteSpace(data.password) || data.password.Length < 6 || data.password.Length > 10)
                    return ErrorResult(ResultCode.Failed, "密码不能为空且长度必须6-10位！");
                result = _userService.AddUser(entityUser.id, data);
            }
            else
            {
                result = _userService.UpdateUser(entityUser.id, data);
            }
            if (result.code == ResultCode.Success)
                return SuccessResult();
            else
                return ErrorResult(result.code, result.message);
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteItem(int id)
        {
            UserService _userService = new UserService();
            OperateResult result = _userService.DeleteUser(entityUser.id, id);
            if (result.code == ResultCode.Success)
                return SuccessResult();
            else
                return ErrorResult(result.code, result.message);
        }

        public ActionResult Logout()
        {
            helper.CookieHelper.Remove(".user");
            return RedirectToAction("Index","Home");
        }
    }
}