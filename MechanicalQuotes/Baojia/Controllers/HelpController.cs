﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.tanlingyun.Baojia.Controllers
{
    /// <summary>
    /// 帮助
    /// </summary>
    [UserType(type = new ViewModel.ELevelType[] { ViewModel.ELevelType.Admin,ViewModel.ELevelType.Manager,ViewModel.ELevelType.Operator})]
    public class HelpController : BaseController
    {
        /// <summary>
        /// 操作手册
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
    }
}