﻿using com.tanlingyun.baojia.Entity;
using com.tanlingyun.DbServices;
using com.tanlingyun.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.tanlingyun.Baojia.Controllers
{
    /// <summary>
    /// 员工设置
    /// </summary>
    [UserType(type = new ViewModel.ELevelType[] { ELevelType.Admin, ELevelType.Manager })]
    public class EmployeeController : BaseController
    {
        /// <summary>
        /// 员工设置列表
        /// </summary>
        /// <returns></returns>
        public ActionResult list()
        {
            return View();
        }

        /// <summary>
        /// 分页
        /// </summary>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <param name="search"></param>
        /// <returns></returns>
        public JsonResult listdata(int PageIndex = 1, int PageSize = 10, string search = "",int manager_id = 0)
        {
            EmployeeService _empService = new EmployeeService();
            PageInfo<Employee> rawPage = _empService.GetEmployeeList(entityUser.id, search, PageIndex, PageSize, manager_id);
            return DataPageResult<Employee>(rawPage);
        }

        /// <summary>
        /// 工序员工下拉框
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [UserType(type = new ViewModel.ELevelType[] { ELevelType.Admin, ELevelType.Manager, ELevelType.Operator })]
        public JsonResult all()
        {
            EmployeeService _empService = new EmployeeService();
            PageInfo<Employee> rawPage = _empService.GetEmployeeList(entityUser.id, "", 1, 9999);
            return Json(rawPage.List.Select(x => new { x.id, x.name })); 
        }

        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [Route("adddata")]
        [HttpPost]
        public JsonResult AddData(Employee data)
        {
            if (data == null)
                return ErrorResult(ResultCode.Failed, "数据为空!");
            //验证数据格式
            if (!ModelState.IsValid)
                return ErrorResult(ResultCode.Failed, ModelState.Where(x => x.Value.Errors.Count > 0).FirstOrDefault().Value.Errors.FirstOrDefault().ErrorMessage);

            EmployeeService _empService = new EmployeeService();
            OperateResult result = null;
            if (data.id == 0)
            {
                result = _empService.AddEmployee(entityUser.id, data);
            }
            else
            {
                result = _empService.UpdateEmployee(entityUser.id, data);
            }
            if (result.code == ResultCode.Success)
                return SuccessResult();
            else
                return ErrorResult(result.code, result.message);
        }

        /// <summary>
        /// 删除原材料
        /// </summary>
        /// <param name="raw_id"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteItem(int employee_id)
        {
            if (employee_id == 0)
                return ErrorResult(ResultCode.Failed, "没有选中行!");
            EmployeeService _empService = new EmployeeService();
            OperateResult result = _empService.DeleteEmployee(entityUser.id, employee_id);
            if (result.code == ResultCode.Success)
                return SuccessResult();
            else
                return ErrorResult(result.code, result.message);
        }
    }
}