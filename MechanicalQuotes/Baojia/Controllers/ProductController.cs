﻿using com.tanlingyun.baojia.Entity;
using com.tanlingyun.DbServices;
using com.tanlingyun.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.tanlingyun.Baojia.Controllers
{
    [UserType(type = new ViewModel.ELevelType[] { ViewModel.ELevelType.Admin, ViewModel.ELevelType.Manager, ViewModel.ELevelType.Operator })]
    public class ProductController : BaseController
    {
        /// <summary>
        /// 产品零件材料工艺管理
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 材料清单及汇总
        /// </summary>
        /// <returns></returns>
        public ActionResult Summary()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="search"></param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult listdata(string search = "",int PageIndex=1,int PageSize = 10,int type = 0)
        {
            ProductService _productService = new ProductService();
            PageInfo<Products> pdlist = _productService.plist(entityUser.id, search, PageIndex, PageSize);
            if (type == 0)
                return DataPageResult<Products>(pdlist);
            else
                return Json(new { code = (int)ResultCode.Success, data = pdlist.List.Select(x => new { x.ID, x.Name, x.IsFinished }) });
        }

        /// <summary>
        /// 材料清单及汇总列表数据
        /// </summary>
        /// <param name="search"></param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult summarylistdata(string search = "", int PageIndex = 1, int PageSize = 10, int type = 0)
        {
            ProductService _productService = new ProductService();
            PageInfo<V_ProductSummary> pspage = _productService.psummarylist(entityUser.id, search, PageIndex, PageSize);
            return DataPageResult<V_ProductSummary>(pspage);
        }

        /// <summary>
        /// 获取产品树
        /// </summary>
        /// <param name="parent_id"></param>
        /// <returns></returns>
        public JsonResult GetTree(int parent_id = 0)
        {
            ProductService _productServices = new ProductService();
            IList<V_ProductTree> tree = _productServices.GetProductTree(entityUser.id);
            return Json(new { code = (int)ResultCode.Success, data = tree });
        }

        public ActionResult list()
        {
            return View();
        }
        /// <summary>
        /// 产品零件以及零件工序
        /// </summary>
        /// <param name="partid"></param>
        /// <param name="productid"></param>
        /// <param name="parent_id">product part id</param>
        /// <returns></returns>
        public ActionResult Partlist(int partid,int productid, int parent_id = 0)
        {
            if (parent_id < 0) parent_id = 0;
            return View();
        }

        /// <summary>
        /// 产品零件详情分页
        /// </summary>
        /// <param name="productid"></param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ProductPartlistdata(int productid, int parent_id = 0, int PageIndex = 1,int PageSize = 10)
        {
            if (parent_id < 0) parent_id = 0;
            ProductService _productService = new ProductService();
            PageInfo<V_ProductPart> vpppage = _productService.GetProductPartPage(entityUser.id, productid,parent_id, PageIndex, PageSize);
            return DataPageResult<V_ProductPart>(vpppage);
        }

        /// <summary>
        /// 零件工序
        /// </summary>
        /// <param name="productid"></param>
        /// <param name="partid"></param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        public JsonResult PartGxlistdata(int productid = 0,int pp_id  = 0,int PageIndex=1,int PageSize = 10)
        {
            PartService _partService = new PartService();
            PageInfo<V_list_PartGx> vpppage = _partService.GetPartGx(entityUser.id, productid, pp_id, PageIndex, PageSize);
            return DataPageResult<V_list_PartGx>(vpppage);
        }

        /// <summary>
        /// 零件搜索接口
        /// </summary>
        /// <param name="search"></param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Partlistdata(string search="",int PageIndex = 1,int PageSize = 10)
        {
            ProductService _productService = new ProductService();
            PageInfo<Parts> ppart = _productService.Partlist(entityUser.id, search, PageIndex, PageSize);
            return DataPageResult<Parts>(ppart);
        }

        /// <summary>
        /// 零件材料表
        /// </summary>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult PartMateriallistdata(int productid = 0,int partid = 0,int pp_id  = 0,int PageIndex = 1,int PageSize = 10)
        {
            ProductService _productService = new ProductService();
            PageInfo<V_PartMaterial> vppage = _productService.GetPartMaterial(entityUser.id, productid, partid, pp_id, PageIndex, PageSize);
            foreach (var item in vppage.List)
            {
                item.TotalWeight = Math.Round(item.TotalWeight, 3);
            }
            return DataPageResult<V_PartMaterial>(vppage);
        }

        /// <summary>
        /// 添加零件
        /// </summary>
        /// <param name="product_id">产品编号</param>
        /// <param name="part_id">零件编号(添加是为0)</param>
        /// <param name="parent_part_id">父级零件编号</param>
        /// <param name="sno">零件号</param>
        /// <param name="name">零件名</param>
        /// <returns></returns>
        public JsonResult AddPart(int product_id,int part_id = 0,int parent_part_id = 0,string sno = "",string name = "")
        {
            if (part_id == 0 && (string.IsNullOrWhiteSpace(sno) || string.IsNullOrWhiteSpace(name)))
                return ErrorResult(ResultCode.Failed, "未获取到零件信息，无法添加！");
            if (parent_part_id == 0) parent_part_id = 0;
            
            ProductService _productService = new ProductService();
            OperateResult result = null;
            if (part_id == 0)
                result = _productService.AddPart(entityUser.id, product_id, parent_part_id, new Parts() { Name = name, Sno = sno });
            else
                result = _productService.AddProductPart(entityUser.id, product_id, part_id, parent_part_id);
            if(result!=null)
            {
                if (result.code == ResultCode.Success)
                    return SuccessResult();
                else
                    return ErrorResult(result.code, result.message);
            }
            return ErrorResult(ResultCode.Failed,"产品零件添加失败！");
        }

        /// <summary>
        /// 零件原材料
        /// </summary>
        /// <param name="partid"></param>
        /// <param name="productid"></param>
        /// <returns></returns>
        public ActionResult Materiallist(int partid, int productid,int pp_id = 0)
        {
            ProductService _productService = new ProductService();
            Parts entityPart =  _productService.GetPart(partid);
            return View(entityPart);
        }



        /// <summary>
        /// 搜索零件
        /// </summary>
        /// <returns></returns>
        public ActionResult SearchPart()
        {
            return View();
        }

        /// <summary>
        /// 搜索工序
        /// </summary>
        /// <returns></returns>
        public ActionResult SearchGy()
        {
            return View();
        }

        /// <summary>
        /// 搜索原材料
        /// </summary>
        /// <returns></returns>
        public ActionResult SearchRaw()
        {
            return View();
        }

        /// <summary>
        /// 添加产品
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AddProduct(Products data)
        {
            if (data == null)
                return ErrorResult(ResultCode.Failed, "数据为空!");
            //验证数据格式
            if (!ModelState.IsValid)
                return ErrorResult(ResultCode.Failed, ModelState.Where(x => x.Value.Errors.Count > 0).FirstOrDefault().Value.Errors.FirstOrDefault().ErrorMessage);

            ProductService _productService = new ProductService();
            OperateResult result = _productService.AddProduct(entityUser.id, data);
            if (result.code == ResultCode.Success)
                return SuccessResult();
            else
                return ErrorResult(result.code, result.message);
        }

        /// <summary>
        /// 复制产品
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult CopyProduct(FormCollection collection)
        {
            Products newpd = new Products();
            newpd.OrderNo = collection["OrderNo"];
            newpd.Name = collection["Name"];
            newpd.Sno = collection["Sno"];
            newpd.Customer = collection["Customer"];
            ProductService _productService = new ProductService();
            OperateResult result = _productService.CopyProduct(entityUser.id, int.Parse(collection["product_id"]), newpd);
            if (result.code == ResultCode.Success)
                return SuccessResult();
            else
                return ErrorResult(result.code, result.message);

        }

        /// <summary>
        /// 保存零件工序
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AddGxData(PartGx data)
        {
            if (data == null)
                return ErrorResult(ResultCode.Failed,"数据不能为空！");
            PartService _partService = new PartService();
            OperateResult result = _partService.AddPartGx(entityUser.id, data);
            if (result.code == ResultCode.Success)
                return SuccessResult();
            return ErrorResult(result.code, result.message);
        }

        /// <summary>
        /// 添加零件原材料
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AddPartMaterialGx(MaterialGx data)
        {
            if (data == null)
                return ErrorResult(ResultCode.Failed,"数据为空！");
            if (!ModelState.IsValid)
                return ErrorResult(ResultCode.Failed, ModelState.Where(x => x.Value.Errors.Count > 0).FirstOrDefault().Value.Errors.FirstOrDefault().ErrorMessage);

            PartService _partService = new PartService();
            OperateResult result = _partService.AddMaterialGx(entityUser.id, data);
            if (result.code == ResultCode.Success)
                return SuccessResult();
            return ErrorResult(result.code, result.message);
        }

        /// <summary>
        /// 为零件添加原材料
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public JsonResult AddPartMetarial(PartMaterials data)
        {
            if (data == null)
                return ErrorResult(ResultCode.Failed, "数据为空!");
            //验证数据格式
            //if (!ModelState.IsValid)
            //    return ErrorResult(ResultCode.Failed, ModelState.Where(x => x.Value.Errors.Count > 0).FirstOrDefault().Value.Errors.FirstOrDefault().ErrorMessage);
            ProductService _productService = new ProductService();
            OperateResult result = _productService.AddPartMaterial(entityUser.id,data);
            if (result.code == ResultCode.Success)
                return SuccessResult();
            return ErrorResult(result.code, result.message);
        }

        /// <summary>
        /// 完成产品
        /// </summary>
        /// <param name="product_id"></param>
        /// <returns></returns>
        public JsonResult IsFinished(int product_id)
        {
            ProductService _productService = new ProductService();
            OperateResult result = _productService.IsFinishPD(entityUser.id, product_id);
            if (result.code == ResultCode.Success)
                return SuccessResult();
            return ErrorResult(result.code, result.message);
        }

        /// <summary>
        /// 删除项目
        /// </summary>
        /// <param name="product_id"></param>
        /// <param name="pp_id"></param>
        /// <returns></returns>
        public JsonResult DeleteItem(int product_id,int pp_id)
        {
            ProductService _productService = new ProductService();
            OperateResult result = _productService.DeleteItem(entityUser.id, product_id, pp_id);
            if (result.code == ResultCode.Success)
                return SuccessResult();
            return ErrorResult(result.code, result.message);

        }


    }
}