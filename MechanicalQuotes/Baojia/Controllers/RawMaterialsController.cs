﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using com.tanlingyun.helper;
using com.tanlingyun.baojia.Entity;
using com.tanlingyun.ViewModel;
using com.tanlingyun.DbServices;

namespace com.tanlingyun.Baojia.Controllers
{
    /// <summary>
    /// 原材料
    /// </summary>
    [RoutePrefix("raw")]
    [UserType(type = new ViewModel.ELevelType[] { ELevelType.Admin, ELevelType.Manager })]
    public class RawMaterialsController : BaseController
    {
        /// <summary>
        /// 原材料列表
        /// </summary>
        /// <returns></returns>
        public ActionResult list()
        {
            return View();
        }

        /// <summary>
        /// 分页
        /// </summary>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <param name="search"></param>
        /// <param name="manager_id"></param>
        /// <returns></returns>
        [UserType(type = new ViewModel.ELevelType[] { ELevelType.Admin, ELevelType.Manager, ELevelType.Operator })]
        public JsonResult listdata(int PageIndex = 1,int PageSize = 10,string search = "",int manager_id = 0)
        {
            RawService _rawService = new RawService();
            PageInfo<RawMaterials> rawPage = _rawService.GetRawList(entityUser.id, search, PageIndex, PageSize,manager_id);
            return DataPageResult<RawMaterials>(rawPage);
        }

        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [Route("adddata")]
        [HttpPost]
        public JsonResult AddData(RawMaterials data)
        {
            if (data == null)
                return ErrorResult(ResultCode.Failed,"数据为空!");
            //验证数据格式
            if (!ModelState.IsValid)
                return ErrorResult(ResultCode.Failed, ModelState.Where(x => x.Value.Errors.Count > 0).FirstOrDefault().Value.Errors.FirstOrDefault().ErrorMessage);

            RawService _rawService = new RawService();
            OperateResult result = null;
            if (data.id == 0)
            {
                result = _rawService.AddRaw(entityUser.id, data);
            }
            else
            {
                result = _rawService.UpdateItem(entityUser.id, data);
            }
            if (result.code == ResultCode.Success)
                return SuccessResult();
            else
                return ErrorResult(result.code, result.message);
        }

        /// <summary>
        /// 删除原材料
        /// </summary>
        /// <param name="raw_id"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteItem(int raw_id)
        {
            if (raw_id == 0)
                return ErrorResult(ResultCode.Failed,"没有选中行!");
            RawService _rawService = new RawService();
            OperateResult result = _rawService.DeleteItem(entityUser.id, raw_id);
            if (result.code == ResultCode.Success)
                return SuccessResult();
            else
                return ErrorResult(result.code, result.message);
        }
    }
}