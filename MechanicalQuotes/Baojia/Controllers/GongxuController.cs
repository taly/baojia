﻿using com.tanlingyun.baojia.Entity;
using com.tanlingyun.DbServices;
using com.tanlingyun.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.tanlingyun.Baojia.Controllers
{
    /// <summary>
    /// 工序管理
    /// </summary>
    [UserType(type = new ViewModel.ELevelType[] { ELevelType.Admin,ELevelType.Manager })]
    public class GongxuController : BaseController
    {
        
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 工序分页
        /// </summary>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <param name="search"></param>
        /// <returns></returns>
        public JsonResult listdata(int PageIndex = 1, int PageSize = 10, string search = "",int manager_id = 0)
        {
            GongxuService _gongxuService = new GongxuService();
            PageInfo<Gongxu> plist = _gongxuService.GetPageList(entityUser.id, search, PageIndex, PageSize,manager_id);
            return DataPageResult<Gongxu>(plist);
        }

        /// <summary>
        /// 工序下拉
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [UserType(type = new ViewModel.ELevelType[] { ELevelType.Admin, ELevelType.Manager, ELevelType.Operator })]
        public JsonResult all()
        {
            GongxuService _gongxuService = new GongxuService();
            PageInfo<Gongxu> plist = _gongxuService.GetPageList(entityUser.id, "", 1, 999);
            return Json(plist.List.Select(x => new { x.ID, x.name }));
        }

        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [Route("adddata")]
        [HttpPost]
        public JsonResult AddData(Gongxu data)
        {
            if (data == null)
                return ErrorResult(ResultCode.Failed, "数据为空!");
            //验证数据格式
            if (!ModelState.IsValid)
                return ErrorResult(ResultCode.Failed, ModelState.Where(x => x.Value.Errors.Count > 0).FirstOrDefault().Value.Errors.FirstOrDefault().ErrorMessage);

            GongxuService _gongxuSerice = new GongxuService();

            OperateResult result = null;
            if (data.ID == 0)
            {
                result = _gongxuSerice.AddGongxu(entityUser.id, data);
            }
            else
            {
                result = _gongxuSerice.UpdateGx(entityUser.id, data);
            }
            if (result.code == ResultCode.Success)
                return SuccessResult();
            else
                return ErrorResult(result.code, result.message);
        }

        /// <summary>
        /// 删除工序
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteItem(int id)
        {
            GongxuService _gongxuService = new GongxuService();
            OperateResult result = _gongxuService.DeleteGx(entityUser.id, id);
            if (result.code == ResultCode.Success)
                return SuccessResult();
            else
                return ErrorResult(result.code, result.message);
        }
    }
}