﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.tanlingyun.Baojia.Controllers
{
    public class HomeController : BaseController
    {
        public PartialViewResult Top()
        {
            if (entityUser.level_type == (int)ViewModel.ELevelType.Operator)
                return PartialView("Top1");
            return PartialView();
        }

        public ActionResult Index()
        {
            if (entityUser.level_type == (int)ViewModel.ELevelType.Operator)
                return RedirectToAction("Index", "Product");
            return RedirectToAction("list", "RawMaterials");
            //return View();
        }
    }
}