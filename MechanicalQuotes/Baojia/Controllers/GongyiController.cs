﻿using com.tanlingyun.baojia.Entity;
using com.tanlingyun.DbServices;
using com.tanlingyun.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.tanlingyun.Baojia.Controllers
{
    /// <summary>
    /// 工艺术语
    /// </summary>
    [UserType(type = new com.tanlingyun.ViewModel.ELevelType[] { ViewModel.ELevelType.Admin,ViewModel.ELevelType.Manager })]
    public class GongyiController : BaseController
    {
        /// <summary>
        /// 工艺术语
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 分页
        /// </summary>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <param name="search"></param>
        /// <returns></returns>
        [HttpPost]
        [UserType(type = new com.tanlingyun.ViewModel.ELevelType[] { ViewModel.ELevelType.Admin, ViewModel.ELevelType.Manager, ELevelType.Operator })]
        public JsonResult listdata(int PageIndex = 1, int PageSize = 10, string search = "", int manager_id = 0)
        {
            GongyiService _gongyiService = new GongyiService();
            PageInfo<Shuyu> plist = _gongyiService.list(entityUser.id, search, PageIndex, PageSize, manager_id);
            return DataPageResult<Shuyu>(plist);
        }

        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [Route("adddata")]
        [HttpPost]
        public JsonResult AddData(Shuyu data)
        {
            if (data == null)
                return ErrorResult(ResultCode.Failed, "数据为空!");
            //验证数据格式
            if (!ModelState.IsValid)
                return ErrorResult(ResultCode.Failed, ModelState.Where(x => x.Value.Errors.Count > 0).FirstOrDefault().Value.Errors.FirstOrDefault().ErrorMessage);

            GongyiService _gongyiSerice = new GongyiService();

            OperateResult result = null;
            if (data.ID == 0)
            {
                result = _gongyiSerice.AddShuyu(entityUser.id, data);
            }
            else
            {
                result = _gongyiSerice.UpdateShuyu(entityUser.id, data);
            }
            if (result.code == ResultCode.Success)
                return SuccessResult();
            else
                return ErrorResult(result.code, result.message);
        }

        /// <summary>
        /// 删除工艺术语
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteItem(int id)
        {
            GongyiService _gongyiService = new GongyiService();
            OperateResult result = _gongyiService.DeleteShuyu(entityUser.id, id);
            if (result.code == ResultCode.Success)
                return SuccessResult();
            else
                return ErrorResult(result.code, result.message);
        }
    }
}