﻿using com.tanlingyun.baojia.Entity;
using com.tanlingyun.DbServices;
using com.tanlingyun.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.tanlingyun.Baojia.Controllers
{
    [UserType(type = new ViewModel.ELevelType[] { ViewModel.ELevelType.Admin, ViewModel.ELevelType.Manager })]
    public class PriceController : BaseController
    {
        /// <summary>
        /// 产品价格
        /// </summary>
        /// <returns></returns>
        public ActionResult ProductPrice()
        {
            return View();
        }

        /// <summary>
        /// 历史记录
        /// </summary>
        /// <returns></returns>
        public ActionResult HistoryPrice()
        {
            return View();
        }

        [HttpPost]
        public JsonResult PriceRecordlistdata(string search= "",int PageIndex=1,int PageSize = 10)
        {
            PriceService _priceService = new PriceService();
            PageInfo<V_list_pricerecords> prdpage = _priceService.GetPriceRecord(entityUser.id, search, PageIndex, PageSize);
            return DataPageResult<V_list_pricerecords>(prdpage);
        }

        /// <summary>
        /// 调差记录
        /// </summary>
        /// <returns></returns>
        public ActionResult TiaoCha()
        {
            return View();
        }

        [HttpPost]
        public JsonResult TClistdata(string time,int PageIndex = 1,int PageSize= 10,int manager_id = 0)
        {
            ProductService _productService = new ProductService();
            PageInfo<Products> pdlist = _productService.plist(entityUser.id, "", PageIndex, PageSize,manager_id);
            return DataPageResult<Products>(pdlist);
        }

    }
}