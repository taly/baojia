﻿using com.tanlingyun.baojia.Entity;
using com.tanlingyun.DbServices;
using com.tanlingyun.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.tanlingyun.Baojia.Controllers
{
    /// <summary>
    /// 外购件及标准件及辅助件
    /// </summary>
    [UserType(type = new ViewModel.ELevelType[] { ViewModel.ELevelType.Admin, ViewModel.ELevelType.Manager })]
    public class OtherRmController : BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 分页
        /// </summary>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <param name="search"></param>
        /// <returns></returns>
        public JsonResult listdata(int PageIndex = 1, int PageSize = 10, string search = "",int manager_id = 0)
        {
            OtherMaterialsService _otherRmService = new OtherMaterialsService();
            PageInfo<OtherRm> plist = _otherRmService.GetOtherRmPage(entityUser.id, search, PageIndex, PageSize, manager_id);
            return DataPageResult<OtherRm>(plist);
        }

        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [Route("adddata")]
        [HttpPost]
        public JsonResult AddData(OtherRm data)
        {
            if (data == null)
                return ErrorResult(ResultCode.Failed, "数据为空!");
            //验证数据格式
            if (!ModelState.IsValid)
                return ErrorResult(ResultCode.Failed, ModelState.Where(x => x.Value.Errors.Count > 0).FirstOrDefault().Value.Errors.FirstOrDefault().ErrorMessage);

            OtherMaterialsService _otherRmService = new OtherMaterialsService();

            OperateResult result = null;
            if (data.ID == 0)
            {
                result = _otherRmService.AddOther(entityUser.id, data);
            }
            else
            {
                result = _otherRmService.UpdateOther(entityUser.id, data);
            }
            if (result.code == ResultCode.Success)
                return SuccessResult();
            else
                return ErrorResult(result.code, result.message);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteItem(int id)
        {
            OtherMaterialsService _otherRmService = new OtherMaterialsService();
            OperateResult result = _otherRmService.DelteItem(entityUser.id, id);
            if (result.code == ResultCode.Success)
                return SuccessResult();
            else
                return ErrorResult(result.code, result.message);
        }
    }
}