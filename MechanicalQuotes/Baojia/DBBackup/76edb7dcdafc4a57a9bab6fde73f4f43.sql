-- MySQL dump 10.16  Distrib 10.1.25-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: Baojia
-- ------------------------------------------------------
-- Server version	10.1.25-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `backuprecords`
--

DROP TABLE IF EXISTS `backuprecords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `backuprecords` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `Time` datetime DEFAULT NULL,
  `Backup_user` int(11) NOT NULL,
  `Backup_file` varchar(500) NOT NULL,
  `Status` int(11) NOT NULL,
  `Version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `backuprecords`
--

LOCK TABLES `backuprecords` WRITE;
/*!40000 ALTER TABLE `backuprecords` DISABLE KEYS */;
INSERT INTO `backuprecords` VALUES (1,'',NULL,0,'',0,0),(2,'asdds','2017-10-14 16:26:27',1,'D:\\Business\\机械加工报价系统\\Code\\MechanicalQuotes\\Baojia\\DBBackup/8482df5dd5834851a54e1d1ac5abb309.sql',1,0),(3,'asdds','2017-10-14 16:49:28',1,'D:\\Business\\机械加工报价系统\\Code\\MechanicalQuotes\\Baojia\\DBBackup/76edb7dcdafc4a57a9bab6fde73f4f43.sql',1,0);
/*!40000 ALTER TABLE `backuprecords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  `alias` varchar(50) NOT NULL,
  `chejian` varchar(50) NOT NULL,
  `gongzhong` varchar(50) NOT NULL,
  `contract` varchar(50) NOT NULL,
  `remark` tinytext,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1,'1w','2','4','3','5','6',0);
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gongxu`
--

DROP TABLE IF EXISTS `gongxu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gongxu` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `price` decimal(18,2) NOT NULL,
  `isCalWeight` tinyint(4) NOT NULL,
  `isDealHot` tinyint(4) NOT NULL,
  `isDealSurface` tinyint(4) NOT NULL,
  `remark` tinytext,
  `alias` varchar(50) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gongxu`
--

LOCK TABLES `gongxu` WRITE;
/*!40000 ALTER TABLE `gongxu` DISABLE KEYS */;
INSERT INTO `gongxu` VALUES (1,'1',2.00,0,0,0,NULL,NULL,0),(2,'11',2.00,1,0,0,'2222',NULL,1),(3,'31',1.00,0,0,0,NULL,NULL,1);
/*!40000 ALTER TABLE `gongxu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `otherrm`
--

DROP TABLE IF EXISTS `otherrm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `otherrm` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SNO` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `alias` varchar(50) DEFAULT NULL,
  `model` varchar(50) DEFAULT NULL,
  `guige` varchar(50) DEFAULT NULL,
  `unit` varchar(20) DEFAULT NULL,
  `price` decimal(18,2) DEFAULT NULL,
  `supplier` varchar(50) DEFAULT NULL,
  `contact` varchar(50) DEFAULT NULL,
  `contactnum` varchar(50) DEFAULT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SNO` (`SNO`),
  KEY `name` (`name`),
  KEY `alias` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `otherrm`
--

LOCK TABLES `otherrm` WRITE;
/*!40000 ALTER TABLE `otherrm` DISABLE KEYS */;
INSERT INTO `otherrm` VALUES (1,'达到','阿斯蒂芬','12','▽dad↗','12±d','米',12.00,'da','23','aesf',1),(2,'1','2','3','4','5','6',7.00,'8','9','0',0);
/*!40000 ALTER TABLE `otherrm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Sno` varchar(50) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Customer` varchar(20) DEFAULT NULL,
  `OrderNo` varchar(50) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `IsFinished` int(11) DEFAULT NULL,
  `add_user` int(11) DEFAULT NULL,
  `addtime` datetime DEFAULT NULL,
  `FinishTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Sno` (`Sno`),
  KEY `Name` (`Name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'1','2','3','4',5,6,7,'2017-10-25 00:00:00',NULL),(2,'1','2','3','4',1,0,1,'2017-10-09 13:35:09',NULL),(3,'tk_001','模具1','王⑤','12',1,0,1,'2017-10-09 13:43:54',NULL);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rawmaterials`
--

DROP TABLE IF EXISTS `rawmaterials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rawmaterials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `alias` varchar(50) NOT NULL,
  `guige` varchar(100) NOT NULL,
  `price` decimal(18,2) NOT NULL,
  `tfl_price` decimal(18,2) NOT NULL,
  `bizhong` float NOT NULL,
  `time` datetime NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rawmaterials`
--

LOCK TABLES `rawmaterials` WRITE;
/*!40000 ALTER TABLE `rawmaterials` DISABLE KEYS */;
INSERT INTO `rawmaterials` VALUES (2,'35#钢板','35#gb','Φ30-Φ50',5.20,2.10,7.85,'2019-09-25 00:00:00',1),(5,'121','啊师傅','323',21.00,23.00,8.98,'2020-09-27 00:00:00',1),(6,'245','3','4',5.00,6.00,7,'2017-09-26 00:00:00',1),(7,'2±±⊥','3','4gg',5.00,6.00,7,'2017-09-26 00:00:00',1),(8,'的','dad','ads',1.00,1.00,1,'2017-09-25 00:00:00',1);
/*!40000 ALTER TABLE `rawmaterials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shuyu`
--

DROP TABLE IF EXISTS `shuyu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shuyu` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `Gyshuyu` varchar(50) NOT NULL,
  `alias` varchar(50) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shuyu`
--

LOCK TABLES `shuyu` WRITE;
/*!40000 ALTER TABLE `shuyu` DISABLE KEYS */;
INSERT INTO `shuyu` VALUES (6,'1','1','2',0),(7,'车','车全部','c',1);
/*!40000 ALTER TABLE `shuyu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(10) DEFAULT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(32) NOT NULL,
  `name` varchar(10) DEFAULT NULL,
  `isgrant` tinyint(4) DEFAULT '0',
  `level_type` tinyint(4) DEFAULT '0',
  `parentid` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `username` (`username`),
  KEY `userid` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','admin','123456','admin',1,3,0,1),(2,'w','test','123456','12',1,1,0,1),(3,'1','2','123456','3',0,1,0,0),(4,'12','12','123456','23',0,1,0,0),(6,'1','2','123456','3',0,1,1,1),(7,'1','2','123456','2323',0,1,1,1),(8,'1','2adad','123456','23234',0,1,1,0),(9,'1','12','123456','12',0,1,1,0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usertoken`
--

DROP TABLE IF EXISTS `usertoken`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usertoken` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(32) NOT NULL,
  `refresh_time` datetime NOT NULL,
  `expired_time` datetime NOT NULL,
  `last_ip` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usertoken`
--

LOCK TABLES `usertoken` WRITE;
/*!40000 ALTER TABLE `usertoken` DISABLE KEYS */;
INSERT INTO `usertoken` VALUES (1,1,'944968001084494f8c6f649df1e9a8ac','2017-10-12 22:38:23','2017-10-19 22:38:23',NULL),(2,2,'8ff40ee429074f3aa07614e825a28e21','2017-10-07 18:22:52','2017-10-14 18:22:52',NULL);
/*!40000 ALTER TABLE `usertoken` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-14 16:49:29
