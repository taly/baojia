﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.baojia.Entity
{
    /// <summary>
    /// 工序
    /// </summary>
    public class Gongxu
    {
        /// <summary>
        /// 工序编号
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 工序名称
        /// </summary>
        [Required(ErrorMessage ="工序名称不能为空！")]
        [MaxLength(50,ErrorMessage = "工序名称长度不能超过50个字符!")]
        public string name { get; set; }

        /// <summary>
        /// 单价（元/每小时）
        /// </summary>
        [Required(ErrorMessage ="单价不能为空！")]
        public decimal price { get; set; }

        /// <summary>
        /// 是否按重量算加工费
        /// </summary>
        public bool isCalWeight { get; set; }

        /// <summary>
        /// 是否热处理
        /// </summary>
        public bool isDealHot { get; set; }

        /// <summary>
        /// 是否表面处理
        /// </summary>
        public bool isDealSurface { get; set; }

        /// <summary>
        /// 备注说明
        /// </summary>
        public string remark { get; set; }

        /// <summary>
        /// 拼音助记
        /// </summary>
        public string alias { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int status { get; set; }

        /// <summary>
        /// 分管理员编号
        /// </summary>
        public int manager_id { get; set; }

    }
}
