﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.baojia.Entity
{
    public class UserToken
    {
        public int id { get; set; }
        /// <summary>
        /// 用户编号（用户表id）
        /// </summary>
        public int user_id { get; set; }
        /// <summary>
        /// token
        /// </summary>
        public string token { get; set; }
        /// <summary>
        /// 刷新时间
        /// </summary>
        public DateTime refresh_time { get; set; }
        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTime expired_time { get; set; }
        /// <summary>
        /// 最后登录ip
        /// </summary>
        public string last_ip { get; set; }
    }
}
