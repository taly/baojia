﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.baojia.Entity
{
    /// <summary>
    /// 员工
    /// </summary>
    public class Employee
    {
        public int id { get; set; }
        /// <summary>
        /// 员工姓名
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 助记符
        /// </summary>
        public string alias { get; set; }
        /// <summary>
        /// 车间
        /// </summary>
        public string chejian { get; set; }
        /// <summary>
        /// 工种
        /// </summary>
        public string gongzhong { get; set; }
        /// <summary>
        /// 联系方式
        /// </summary>
        public string contract { get; set; }
        /// <summary>
        /// 备注说明
        /// </summary>
        public string remark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int status { get; set; }

        public int manager_id { get; set; }

    }
}
