﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.baojia.Entity
{
    /// <summary>
    /// 零件
    /// </summary>
    public class Parts
    {

        public int ID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Sno { get; set; }

        /// <summary>
        /// 零件名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int status { get; set; }
        /// <summary>
        /// 添加人
        /// </summary>
        public int add_user { get; set; }
        /// <summary>
        /// 添加时间
        /// </summary>
        public DateTime add_time { get; set; }

        /// <summary>
        /// 分管理员账号
        /// </summary>
        public int manager_id { get; set; }

    }
}
