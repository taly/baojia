﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.baojia.Entity
{
    public class User
    {
        public int id { get; set; }

        [MaxLength(10,ErrorMessage = "用户编号长度不能超过10个字符！")]
        public string userid { get; set; }

        [Required(ErrorMessage = "用户名不能为空！")]
        [MaxLength(10,ErrorMessage = "用户名长度不能超过10个字符！")]
        public string username { get; set; }

        [Required(ErrorMessage = "名称不能为空！")]
        [StringLength(10,ErrorMessage = "姓名长度不能超过10个字符！")]
        public string name { get; set; }

        [StringLength(10, MinimumLength = 6, ErrorMessage = "密码长度必须6-10位！")]
        public string password { get; set; }

        public int isgrant { get; set; }

        public int level_type { get; set; }

        public int parentid { get; set; }

        public int status { get; set; }

        /// <summary>
        /// 是否初始化数据(默认0)
        /// </summary>
        public int isinitdata { get; set; }
    }
}
