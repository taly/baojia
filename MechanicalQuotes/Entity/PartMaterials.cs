﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.baojia.Entity
{
    /// <summary>
    /// 零件原材料
    /// </summary>
    public class PartMaterials
    {
        public int ID { get; set; }
        /// <summary>
        /// 原材料编号
        /// </summary>
        [Required(ErrorMessage ="材料编号不能为空！")]
        public int raw_id { get; set; }
        /// <summary>
        /// 下单数量
        /// </summary>
        [Required(ErrorMessage = "下单数量不能为空！")]
        [Range(1,9999,ErrorMessage = "下单数量应该介于1-9999之间")]
        public int qty { get; set; }
        /// <summary>
        /// 产品编号
        /// </summary>
        [Required(ErrorMessage = "产品编号不能为空！")]
        public int product_id { get; set; }
        /// <summary>
        /// 零件编号
        /// </summary>
        [Required(ErrorMessage ="零件编号不能为空！")]
        public int part_id { get; set; }

        public int pp_id { get; set; }

        /// <summary>
        /// 材料类型
        /// </summary>
        [Required(ErrorMessage = "材料型号不能为空！")]
        public int type { get; set; }

        /// <summary>
        /// 板材：厚度(mm)
        /// 棒料：直径(mm)
        /// 圆管料：外径(mm)
        /// 方管料：长(mm)
        /// </summary>
        public double p1 { get; set; }
        /// <summary>
        /// 板材：长(mm)
        /// 棒料：长(mm)
        /// 圆管料：内径(mm)
        /// 方管料：宽(mm)
        /// </summary>
        public double p2 { get; set; }
        /// <summary>
        /// 板材：宽(mm)
        /// 圆管料：长度(mm)
        /// 方管料：壁厚(mm)
        /// </summary>
        public double p3 { get; set; }
        /// <summary>
        /// 方管料：长(mm)
        /// </summary>
        public double p4 { get; set; }

        /// <summary>
        /// 规格(主要针对特殊钢，毛胚和不规则材料)
        /// </summary>
        public string guige { get; set; }

        /// <summary>
        /// 单重
        /// </summary>
        public double weight { get; set; }

        /// <summary>
        /// 退废料重量
        /// </summary>
        public double tfl_weight { get; set; }

        /// <summary>
        /// 表面积
        /// </summary>
        public double SurfaceArea { get; set; }

        public int add_user { get; set; }

        public DateTime add_time { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int status { get; set; }

        /// <summary>
        /// 原材料加工价格
        /// </summary>
        public decimal price { get; set; }

        /// <summary>
        /// 最终价格
        /// </summary>
        public decimal total_price { get; set; }
    }
}
