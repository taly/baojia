﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.baojia.Entity
{
    /// <summary>
    /// 原材料
    /// </summary>
    public class RawMaterials
    {
        public int id { get; set; }
        /// <summary>
        /// 原材料名
        /// </summary>
        [Required(ErrorMessage ="材料名不能为空!")]
        [MaxLength(255, ErrorMessage = "材料名不能超过50个字符")]
        public string name { get; set; }
        /// <summary>
        /// 拼音助记
        /// </summary>
        [MaxLength(50, ErrorMessage = "拼音助记不能超过50个字符")]
        public string alias { get; set; }
        /// <summary>
        /// 规格
        /// </summary>
        public string guige { get; set; }
        /// <summary>
        /// 单价
        /// </summary>
        [Range(typeof(decimal),"0","9999",ErrorMessage ="材料单价值应该在0-9999之间!")]
        
        public decimal price { get; set; }
        /// <summary>
        /// 退废料单价
        /// </summary>
        [Range(0, 9999, ErrorMessage = "退废料单价值应该在0-9999之间!")]
        public decimal tfl_price { get; set; }
        /// <summary>
        /// 比重
        /// </summary>
        [Range(0, 100, ErrorMessage = "比重应该在0-100之间!")]
        public float bizhong { get; set; }
        /// <summary>
        /// 日期
        /// </summary>
        public DateTime time { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int status { get; set; }

        /// <summary>
        /// 分管理员账号(0表示默认)
        /// </summary>
        public int manager_id { get; set; }
    }
}
