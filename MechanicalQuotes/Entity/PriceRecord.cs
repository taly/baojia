﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.baojia.Entity
{
    /// <summary>
    /// 价格记录
    /// </summary>
    public class PriceRecords
    {
        public int ID { get; set; }

        public int product_id { get; set; }

        public decimal price { get; set; }

        /// <summary>
        /// 材料价格
        /// </summary>
        public decimal material_price { get; set; }
        /// <summary>
        /// 加工费
        /// </summary>
        public decimal ProcessingFees { get; set; }
        /// <summary>
        /// 利润率
        /// </summary>
        public float Profit { get; set; }


        public double weight { get; set; }

        /// <summary>
        /// 操作人
        /// </summary>
        public int operator_user { get; set; }

        /// <summary>
        /// 操作时间
        /// </summary>
        public DateTime operator_time { get; set; }
    }
}
