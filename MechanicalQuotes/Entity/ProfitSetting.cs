﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.baojia.Entity
{
    /// <summary>
    /// 利润设置
    /// </summary>
    public class ProfitSetting
    {
        public int ID { get; set; }

        public int MateTimeType { get; set; }

        /// <summary>
        /// 材料与工时成本合计(起始值)
        /// </summary>
        public decimal MateTimeFr { get; set; }
        /// <summary>
        /// 材料与工时成本合计(结束值)
        /// </summary>
        public decimal MateTimeTo { get; set; }

        /// <summary>
        /// 产品月供数量类型
        /// </summary>
        public int PdMonQtyType { get; set; }

        /// <summary>
        /// 产品月供数量(起始值)
        /// </summary>
        public int PdMonQtyFr { get; set; }
        /// <summary>
        /// 产品月供数量(结束值)
        /// </summary>
        public int PdMonQtyTo { get; set; }

        /// <summary>
        /// 材料成本占比
        /// </summary>
        public int MatRatioType { get; set; }
        /// <summary>
        /// 材料成本占比(起始值)
        /// </summary>
        public float MatRatioFr { get; set; }
        /// <summary>
        /// 材料成本占比(结束值)
        /// </summary>
        public float MatRatioTo { get; set; }

        /// <summary>
        /// 利润
        /// </summary>
        public double Profit { get; set; }
        /// <summary>
        /// 分管理员
        /// </summary>
        public int manager_id { get; set; }
    }
}
