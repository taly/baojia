﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.baojia.Entity
{
    public class Products
    {
        public int ID { get; set; }
        /// <summary>
        /// 产品编号
        /// </summary>
        public string Sno { get; set; }
        /// <summary>
        /// 产品名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 客户姓名
        /// </summary>
        public string Customer { get; set; }
        /// <summary>
        /// 订单编号
        /// </summary>
        public string OrderNo { get; set; }
        /// <summary>
        /// 产品状态
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 是否录完
        /// </summary>
        public int IsFinished { get; set; }
        /// <summary>
        /// 添加人
        /// </summary>
        public int add_user { get; set; }

        /// <summary>
        ///  交货期限
        /// </summary>
        public DateTime? FinishTime { get; set; }

        /// <summary>
        /// 添加时间
        /// </summary>
        public DateTime addtime { get; set; }

        /// <summary>
        /// 产品价格
        /// </summary>
        public decimal price { get; set; }

        /// <summary>
        /// 材料价格
        /// </summary>
        public decimal material_price { get; set; }
        /// <summary>
        /// 加工费
        /// </summary>
        public decimal ProcessingFees { get; set; }
        /// <summary>
        /// 利润率
        /// </summary>
        public float Profit { get; set; }

        public double Weight { get; set; }

        /// <summary>
        /// 分管理员
        /// </summary>
        public int manager_id { get; set; }
    }
}
