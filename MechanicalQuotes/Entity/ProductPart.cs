﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.baojia.Entity
{
    /// <summary>
    /// 产品零件
    /// </summary>
    public class ProductPart
    {
        public int ID { get; set; }
        /// <summary>
        /// 产品编号
        /// </summary>
        public int product_id { get; set; }
        /// <summary>
        /// 零件编号
        /// </summary>
        public int part_id { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int status { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public int qty { get; set; }

        /// <summary>
        /// 毛重
        /// </summary>
        public double weight { get; set; }
        /// <summary>
        /// 操作人
        /// </summary>
        public int operator_user { get; set; }
        /// <summary>
        /// 操作时间
        /// </summary>
        public DateTime operator_time { get; set; }

        /// <summary>
        /// 父级零件
        /// </summary>
        public int parent_id { get; set; }
    }
}
