﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.baojia.Entity
{
    /// <summary>
    /// 零件工序
    /// </summary>
    public class PartGx
    {
        public int ID { get; set; }
        /// <summary>
        /// 产品编号
        /// </summary>
        public int product_id { get; set; }
        /// <summary>
        /// 零件编号
        /// </summary>
        public int part_id { get; set; }

        [Required(ErrorMessage = "零件编号不能为空！")]
        public int pp_id { get; set; }

        /// <summary>
        /// 工序号
        /// </summary>
        public string GxSno { get; set; }
        /// <summary>
        /// 工序编号
        /// </summary>
        [Required(ErrorMessage = "工序编号不能为空！")]
        public int gx_id { get; set; }
        /// <summary>
        /// 工艺编号
        /// </summary>
        [Required(ErrorMessage = "工艺术语不能为空！")]
        public int gy_id { get; set; }
        /// <summary>
        /// 准备工时
        /// </summary>
        public double prepare_time { get; set; }
        /// <summary>
        /// 单件工时
        /// </summary>
        public double single_time { get; set; }
        /// <summary>
        /// 热重比
        /// </summary>
        public double hot_percent { get; set; }

        /// <summary>
        /// 总加工费
        /// </summary>
        public decimal totalFee { get; set; }
        /// <summary>
        /// 职工编号
        /// </summary>
        public int employee_id { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int status { get; set; }
    }
}
