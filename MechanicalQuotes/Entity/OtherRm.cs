﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.baojia.Entity
{
    public class OtherRm
    {
        /// <summary>
        /// 其他材料编号
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        [MaxLength(50, ErrorMessage = "物料编号不能超过50个字符长度!")]
        public string SNO { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        [Required(ErrorMessage = "物料名称不能为空！")]
        [MaxLength(50,ErrorMessage = "物料名称不能超过50个字符!")]
        public string name { get; set; }

        /// <summary>
        /// 助记符
        /// </summary>
        [MaxLength(50,ErrorMessage ="助记符不能超过50个字符长度!")]
        public string alias { get; set; }

        /// <summary>
        /// 型号
        /// </summary>
        [MaxLength(50, ErrorMessage = "型号不能超过50个字符长度!")]
        public string model { get; set; }
        /// <summary>
        /// 规格
        /// </summary>
        [MaxLength(50, ErrorMessage = "规格不能超过50个字符长度!")]
        public string guige { get; set; }
        /// <summary>
        /// 单位
        /// </summary>
        [MaxLength(20, ErrorMessage = "单位不能超过20个字符长度!")]
        public string unit { get; set; }
        /// <summary>
        /// 单价
        /// </summary>
        [Required(ErrorMessage = "单价不能为空！")]
        public decimal price { get; set; }
        /// <summary>
        /// 供应商
        /// </summary>
        [MaxLength(50, ErrorMessage = "供应商不能超过50个字符长度!")]
        public string supplier { get; set; }

        /// <summary>
        /// 联系人
        /// </summary>
        [MaxLength(50, ErrorMessage = "联系人不能超过50个字符长度!")]
        public string contact { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
        [MaxLength(50, ErrorMessage = "联系电话不能超过50个字符长度!")]
        public string contactnum { get; set; }

        public int status { get; set; }

        /// <summary>
        /// 分管理员编号
        /// </summary>
        public int manager_id { get; set; }
    }
}
