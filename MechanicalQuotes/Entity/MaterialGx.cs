﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.baojia.Entity
{
    /// <summary>
    /// 材料工序
    /// </summary>
    public class MaterialGx
    {
        public int ID { get; set; }
        /// <summary>
        /// 产品编号
        /// </summary>
        public int product_id { get; set; }
        /// <summary>
        /// 零件编号
        /// </summary>
        public int part_id { get; set; }

        /// <summary>
        /// 产品零件编号
        /// </summary>
        public int pp_id { get; set; }

        /// <summary>
        /// 工序号
        /// </summary>
        public string GxSno { get; set; }
        /// <summary>
        /// 工序编号
        /// </summary>
        public int gx_id { get; set; }
        /// <summary>
        /// 工艺编号
        /// </summary>
        public int gy_id { get; set; }
        /// <summary>
        /// 准备工时
        /// </summary>
        public double prepare_time { get; set; }
        /// <summary>
        /// 单件工时
        /// </summary>
        public double single_time { get; set; }
        /// <summary>
        /// 热重比
        /// </summary>
        public double hot_percent { get; set; }

        /// <summary>
        /// 总加工费
        /// </summary>
        public decimal totalFee { get; set; }

        /// <summary>
        /// 职工编号
        /// </summary>
        public int employee_id { get; set; }
        /// <summary>
        /// 零件材料编号
        /// </summary>
        public int pm_id { get; set; }
    }
}
