﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.baojia.Entity
{
    public class Log
    {
        public int ID { get; set; }

        public int user_id { get; set; }

        public string data { get; set; }

        public string detail { get; set; }
        /// <summary>
        /// 时间
        /// </summary>
        public DateTime time { get; set; }
    }
}
