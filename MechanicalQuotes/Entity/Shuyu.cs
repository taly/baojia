﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.baojia.Entity
{
    /// <summary>
    /// 工艺术语
    /// </summary>
    public class Shuyu
    {
        public int ID { get; set; }

        /// <summary>
        /// 工艺名称
        /// </summary>
        [Required(ErrorMessage = "工艺名称不能为空！")]
        [MaxLength(50,ErrorMessage = "工艺名称长度不能超过50个字符!")]
        public string name { get; set; }

        /// <summary>
        /// 工艺术语
        /// </summary>
        [Required(ErrorMessage = "工艺术语不能为空！")]
        [MaxLength(50,ErrorMessage = "工艺术语长度不能超过50个字符！")]
        public string Gyshuyu { get; set; }

        /// <summary>
        /// 拼音助记
        /// </summary>
        [MaxLength(50,ErrorMessage = "拼音助记长度不能超过50个字符！")]
        public string alias { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int status { get; set; }

        /// <summary>
        /// 分管理员编号
        /// </summary>
        public int manager_id { get; set; }

    }
}
