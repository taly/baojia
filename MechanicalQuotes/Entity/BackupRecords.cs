﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.baojia.Entity
{
    /// <summary>
    /// 备份记录
    /// </summary>
    public class BackupRecords
    {
        public int ID { get; set; }

        /// <summary>
        /// 备份名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 数据库版本
        /// </summary>
        public int Version { get; set; }

        /// <summary>
        /// 备份时间
        /// </summary>
        public DateTime Time { get; set; }
        /// <summary>
        /// 备份人
        /// </summary>
        public int Backup_user { get; set; }

        /// <summary>
        /// 备份文件
        /// </summary>
        public string Backup_file { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; set; }

    }
}
