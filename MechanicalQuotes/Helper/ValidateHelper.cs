﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.helper
{
    public class ValidateHelper
    {
        /// <summary>
        /// 验证邮箱
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static bool IsEmail(string email)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(email, "^(\\w)+(\\.\\w+)*@(\\w)+((\\.\\w+)+)$");
        }

        /// <summary>
        /// 验证手机号码
        /// </summary>
        /// <param name="mobile"></param>
        /// <returns></returns>
        public static bool IsMobile(string mobile)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(mobile, "^1(3|4|5|7|8|9)\\d{9}$");
        }

        /// <summary>
        /// 验证用户名
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public static bool CheckUsername(string username)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(username, "^[0-9A-Za-z]{4,10}$");
        }
    }
}
