﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.helper
{
    public class LogHelper
    {
        public static void SaveNote(string folder,string note)
        {
            SaveNote(folder, DateTime.Now.ToString("yyyy-MM-dd") + ".log", note);
        }

        public static void SaveNote(string folder,string filename,string note)
        {
            string rootDir = AppDomain.CurrentDomain.BaseDirectory + folder;
            if (!System.IO.Directory.Exists(rootDir))
                System.IO.Directory.CreateDirectory(rootDir);
            System.IO.FileStream fs = new System.IO.FileStream(rootDir + "/" + filename, System.IO.FileMode.Append, System.IO.FileAccess.Write, System.IO.FileShare.ReadWrite);
            System.IO.TextWriter tw = new System.IO.StreamWriter(fs);
            tw.WriteLine(note);
            tw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            tw.WriteLine("==========================================================");
            tw.Close();
            fs.Close();
        }
    }
}
