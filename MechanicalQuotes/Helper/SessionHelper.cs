﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace com.tanlingyun.helper
{
    public class SessionHelper
    {
        public static void Add(string name,string value)
        {
            HttpContext.Current.Session.Add(name,value);
        }

        public static T Get<T>(string name)
        {
            return (T)HttpContext.Current.Session[name];
        }
    }
}
