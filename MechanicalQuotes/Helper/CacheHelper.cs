﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace com.tanlingyun.helper
{
    public class CacheHelper
    {
        public static object GetCache(string name)
        {
            return HttpRuntime.Cache.Get(name);
        }
    }
}
