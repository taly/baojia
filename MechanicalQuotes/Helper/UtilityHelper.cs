﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.tanlingyun.helper
{
    public class UtilityHelper
    {
        /// <summary>
        /// 配置字符串值
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        public static string GetConfig(string config)
        {
            return System.Configuration.ConfigurationManager.AppSettings[config] as string;
        }



        /// <summary>
        /// 配置数字值
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        public static int GetIntConfig(string config)
        {
            return int.Parse(System.Configuration.ConfigurationManager.AppSettings[config]);
        }

        /// <summary>
        /// 获取用户加密随机密码
        /// </summary>
        /// <returns></returns>
        public static string GetEncryptKey()
        {
            return Guid.NewGuid().ToString().Replace("-", "").Substring(10, 5);
        }

        public static string DecodeBase64(string text,string encoding = "utf-8")
        {
            return System.Text.Encoding.GetEncoding(encoding).GetString(Convert.FromBase64String(text));
        }

        public static string EncodeToBase64(string text,string encoding = "utf-8")
        {
            return Convert.ToBase64String(System.Text.Encoding.GetEncoding(encoding).GetBytes(text));
        }
    }
}
